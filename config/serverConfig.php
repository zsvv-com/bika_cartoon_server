<?php
/*
 * @Date: 2020-07-24 13:58:42
 * @名称: 服务器配置信息
 * @描述: 
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-24 15:39:10
 * @FilePath: /Pica_acg服务器/config/serverConfig.php
 * @最后编辑: 初雪桜
 */
return [
    "TokenResolutionKey" => '÷MGrˆIjzMy40DNd(n˚rzˆ‘¥eTN¥QM¬≈kU÷ΩWVd!Ek0)∂i†)H©%UßIhtœp˜¥4^E˙9ß≥@0sN(®TΩµgcΩkSsD†˚$T75å‘…“jµ(Ωpånˆ9&tUE$∆t©OTIk!´3vc©˚˜S&1@k0YƒI6RR)÷VGM≤œIç40å7M!n√ndttßΩ≈DƒTqm8x…p√9AπLBopHKT√¨2wp)AB9&424JiXßn4nçfçqQ)X7¨©ZSyB&≤q…4N¥HJ˚…7S!œt%å8oTfn¥d5∆myp*O*≥CP˚˜FKπFfj',
    //登陆加密token的密钥
    //!请注意 修改此项目会导致全部用户重新登录token失效
    "TokenResolutionKeySha512" => 'CƒpM®IQ…å∂ßø#PZƒZcgkœv&B∑eCgxaΩO3iFUvCjBnxY˚≥ß¥®∑FZRp#S#JçUG$Ωgzjß2dhA®…b≥hE˚¥4¨†∑¬÷U)≈‘ΩSoh÷πm*˜eV4¥x)œNAIa†d^)^˚d≥)¨)oKsRS*kA´)Z®qa≤Za7´DO43∑ƒ%QCfqW“QsπˆAj^7$w≤…D07u´cPm0wΩ∫%PHq∂©˜¨vOyƒ0Zfπ¬Xhuøˆ˚b‘≤D8≥X´pdNh*∂Ov5d©v¨ZC¬∂J*eß≥r1y%π3´Af¬No2e)9tKs*$*!0kru',
    //token加密验证密钥
    //!请注意 修改此项目会导致全部用户重新登录token失效
    "UserInfo" => [
        "token" => "eyJFbmNyeXB0aW9uIjoiU0hBMjU2IiwidmVyaWZpY2F0aW9uIjoiU0hBNTEyIn0=.YTFjOXBuWm0wMEJLZ1V1QUNBWTZwTXRFNVI4MHQ0a0ozV2NEUVZyTFp4VzNJZ0srcUhhTnV3MVVKSGREU3ZUbmd2dUpGYmFlRm13TFpyVUJzVCtBUlF3RTMxdUxmOXlwQXQrYlJYTVprM2hQS2VveHZvaDd0RVdYdDhYem1pVDJJUXdOQ1FxdHhLRXNtOFhzSUtuQ3RueWFHdHJHVUFPOVp2ay96cUhGb09oZG5FT0s5UFB0WFZxSTNPeGl2WjgrNUs0K3IrV1FNWWtEQ1h3OVR1SFY0Z0VtY0ZGTmIrWDFPQTh6bGEzMzlpcmFvdHFaMlVJag==.796c5169d80ba03d73fadaeb685fe6a37e3b2304fada8469f09283fede3d45bcbd6a56aff6114c02887572d2aa6b9d674258a13cd10552c3d29c2265cd932587",
    ],
];
