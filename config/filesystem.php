<?php
/*
 * @Date: 2020-06-19 11:16:37
 * @名称: 
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑: 初雪桜
 * @LastEditTime: 2020-08-15 23:43:48
 * @FilePath: /服务器/config/filesystem.php
 */

return [
    // 默认磁盘
    'default' => env('filesystem.driver', 'ProjectImages'),
    // 磁盘列表
    'disks'   => [
        'local'  => [
            'type' => 'local',
            'root' => app()->getRuntimePath() . 'storage',
        ],
        'ClassImages' => [
            // 磁盘类型
            'type'       => 'local',
            // 磁盘路径
            'root'       => app()->getRootPath() . 'public/storage',
            // 磁盘路径对应的外部URL路径
            'root'       => app()->getRootPath() . 'public/public/update/ClassImages',
            // 可见性
            'visibility' => 'public',
        ],
        // 更多的磁盘配置信息
        'ProjectImages' => [ //图片地址
            // 磁盘类型
            'type'       => 'local',
            // 磁盘路径
            'root'       => app()->getRootPath() . 'public/public/update/BookImages',
            // 磁盘路径对应的外部URL路径
            'url'        => '/uploads',
            // 可见性
            'visibility' => 'public',
        ],
        // 更多的磁盘配置信息

        /* 
        @tested 床图文件备份
        @tested 这是一个床图文件丢失时候 备用的 切勿删除 除非确定此文件不再使用 
         */
        'BacktrackingImages' => [
            // 磁盘类型
            'type'       => 'local',
            // 磁盘路径
            'root'       => app()->getRootPath() . 'public/public/Backtracking/BookImages',
            // 磁盘路径对应的外部URL路径
            'url'        => '/uploads',
            // 可见性
            'visibility' => 'public',
        ],
    ],
];
