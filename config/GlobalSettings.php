<?php
/*
 * @Date: 2020-03-07 20:07:08
 * @名称: 全局设置
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-24 17:56:34
 * @FilePath: /服务器/config/GlobalSettings.php
 */
return [
    
    'api_RegUserAuthcode' => 'cefdVLbWiwcQlBZhbZNDDYNACkU8RuYKk0ruYimmg3FJGoGO4koZd0pxKvZb5LGw', //api接口 注册用户 加密用的加密密钥

    'serverLocalhost_Edit_sha256Encryption' => 'GsYSRz@QpD@Z~ya?Ovs$SQITl*q8ZS_b3Z?Zp3R0~JXakq$d4Uc-Lc7~vYVMcH5k', //后台里的查看章节里的全部漫画的sha256 加密密钥
    'serverLocalhost_UpdateController_saverFile' => 'iSAr2qrBW?W-*@!bHuCnmByhui=Z#R2huIU%~_-O+UMNDx+hc^Wi@yKVM8o_@_87', //后台里的上传漫画的加密文件名密钥
    'serverLocalhost_Edit_EditComicsInfo_sha256Encryption' => 'UoaYfG2u4HUXftnX6kBCIV3oi41fVzZ7kLwr47cnaQ78YPDO73KMyeq0HqZutOOQ', //后台里的编辑漫画信息的加密密钥
    'serverLocalhost_ComicsList' => 'DEUQURCcrr5J0YV7&BBRi@nEhx)syhc8vOQ4Q$$pSwkP@2-eZTZ8V$uHcGvjYWp9', //后台里的漫画列表的用户的加密密钥 删除 确认删除 恢复漫画 都需要此密钥解密
    'serverLocalhost_TagsEncryptionKey' => 'g8580qE9VfL":c0\76im(0#[Gn<-$am:&=B-E`5;3pDgb\BBb1`>>B<gW<SCg7m}', //后台里的编辑漫画 的 标签加密的key
    'serverLocalhost_CreateComicsKey' => 'htahiCYf!3sJPpXjSHdm34bsnDT0D!hT!6jWpkK2nm!mtZX55197d*3s5zsQb02D', //后台里的新建漫画 的 验证加密key
    'serverLocalhost_ClassifiedAndTagsEncryptionBase256' => 'vRe4wooVK3H@2A)7qR*e&eqyVkXrue1sWe-D!tmAJr6VJC&diSTVR67HmskCG#nr', //后台里的漫画和标签里的用户token 的加密方式 此处用于第一次加密获取sha256 不可解密 只能对比
    'serverLocalhost_ClassifiedAndTagsEncryptionAuthcode' => 'M~!5bHAyTHNhXZtJQs7qs^7Lc@gaWYcZFAxPZwfBxWV45IinTG+#@RqNtlffnlR@', //后台里的漫画和标签里的用户token 的加密方式 此处用地第二次加密 可解密 使用 Authcode 方法解密 解密出上方数据
    'serverLocalhost_serverHost' => 'serverLocalhost', //后台路径
    'Registered_user_image' => 'https://i0.hdslb.com/bfs/album/037414572522f4e120abfcfc1ad01315f5e375e9.jpg', //用户注册默认头像
    // 'PictureSpringboard' => "https://i0.hdslb.com/bfs/album/c6f8a22da2e8159769240286c590ba30654eb1a4.jpg?url=", //后台路径
    'PictureSpringboard' => "",
    // 'user_info' => [
    //     // "_id" => "user-5af393a7db91d02c83d987b4",
    //     "to_ken" => "eyJFbmNyeXB0aW9uIjoiU0hBMjU2IiwidmVyaWZpY2F0aW9uIjoiU0hBNTEyIn0=.ZGJjYXN1b2hJalVQV0lXRlpONGxiSExQY0ZlQVA3RSt6Q21PUkp6N2Y1eVBZTTlhZW5HSEdmeStRV1FJMnVqZ0VLTjFwL2dFS1R0TDBFVjF1VzdxbGd0OGRoT2NqdTBvSmtyMUIvYXo3dStYMXVhaEIzbDdnL2N5cU1SZGJJa1lKalJqbmhROFZzZlkyb0xJYXp2aXJLcTJIWHpSRjFYQlM2YncwbC8zK0FpYmRXVnpXNk82SDlyTDg3T1ZNQkV5K0d3bHAzdVA1SXlMblhzM1hPUU42bEo0VSsyUXVIMEpJSnA4bDhWUXlUMXMrelJ5SFNQZg==.25cff6a7d9dbe8ec2f1826c64c33f0c61e4d38e9a3e27b5612b2897b0df57ba56cd8c1e1fdd30f431062c2240e20c7d20cc8e8bc15fbb2f6aa3b4c8075a61178",
    // ],
    //!发送邮件的
    'smtpSeverHost' => 'smtp.mail.me.com', //发件服务器地址
    'smtpSeverUser' => 'sakura_223@icloud.com', //发件服务器用户名
    'smtpSeverPass' => 'oroi-dikp-dpoz-xdno', //发件服务器密码
    'smtpSeverMail' => 'sakura_223@icloud.com', //发件服务器发件人地址
    'smtpSeverName' => 'xxx 管理员', //发件服务器发件人名称
    'smtpReplyAddress' => 'sakura_223@icloud.com', //回复地址
    //!发送邮件的
    'image_sever' => 'http://image.sakura.com/static/', //图片服务器地址
    'image_sever_cover' => 'http://9dmeq4.natappfree.cc/cover/', //图片服务器地址
    'host' => 'http://localhost/', //本站域名 /结尾
    'imageTiaoBan' => '', //图片跳板
    //这是 bilibili上传文件的cookie
    "bilibiliUpdateImagesCookie" => "_uuid=5555CA6A-234C-F716-F38B-20A27E8AE55174279infoc; buvid3=0F44B6AF-39EC-48E3-BB0E-E54DFD0EF9E0138386infoc; sid=jalcrn8q; _jct=f579b690df0011eab4bede621ecc6ba5; DedeUserID=12565731; DedeUserID__ckMd5=c7ceae68e50d6a15; SESSDATA=092935b1%2C1613052584%2Cf3043*81; bili_jct=a08634457fe19e410afb8f4d3e503011",
];
