<?php
/*
 * @Date: 2020-05-03 22:03:52
 * @名称: 
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-18 23:35:02
 * @FilePath: /服务器/app/Images/controller/Images.php
 */

namespace app\Images\controller;

use app\BaseController;
use think\facade\Db;
use think\facade\Filesystem;
use think\facade\View;

class Images extends BaseController
{
    public function imageHost1()
    {
        $images_id = input('id');
        $tableNameId = input('tableNameId');
        $info = Db::table($tableNameId)
            ->where([
                "frame_id" =>  $images_id,
            ])
            ->find();
        //判断是不是网络文件

        if (!$info) {
            return redirect("https://i0.hdslb.com/bfs/album/45fadaef0640959f78fb165c5a039d891bf5986b.png"); //所访问的图片服务器没有记录
        }

        if (!$info['is_local_file']) { //是否为本地文件
            return redirect("$info[file_server]$info[path].$info[extension]"); //不是本地文件
        } else {
            try { //如果文件不存在会抛出异常
                $file = Filesystem::disk("ProjectImages")->read("$info[path].$info[extension]");
                return response($file, 200)->contentType('image/gif,image/png,image/jpg,image/jpeg');
            } catch (\Throwable $th) { //抛出异常
                return redirect("https://i0.hdslb.com/bfs/album/bd1bde615215612407258a772c4882fc32819c32.png"); //不存在 提示文件已被删除
            }
        }
    }
    public function cover()
    {
        $images_id = input('id');
        $tableNameId = input('tableNameId');
        $info = Db::table("$tableNameId")
            ->where([
                "frame_id" =>  $images_id,
            ])
            ->find();
        //判断是不是网络文件


        if (!$info) {
            return redirect("https://i0.hdslb.com/bfs/album/45fadaef0640959f78fb165c5a039d891bf5986b.png"); //所访问的图片服务器没有记录
        }

        if (!$info['is_local_file']) { //是否为本地文件
            return redirect("$info[file_server]$info[path]"); //不是本地文件
        } else {
            try { //如果文件不存在会抛出异常
                $file = Filesystem::disk("ProjectImages")->read("$info[path]");
                return response($file, 200)->contentType('image/gif,image/png,image/jpg,image/jpeg');
            } catch (\Throwable $th) { //抛出异常
                return redirect("https://i0.hdslb.com/bfs/album/bd1bde615215612407258a772c4882fc32819c32.png"); //不存在 提示文件已被删除
            }
        }
    }
}
function is_url($v)
{
    $pattern = "#(http|https)://(.*\.)?.*\..*#i";
    if (preg_match($pattern, $v)) {
        return true;
    } else {
        return false;
    }
}
