<?php
/*
 * @Date: 2020-05-03 22:03:52
 * @名称: 
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-17 22:43:42
 * @FilePath: /服务器/app/Images/controller/Images.php
 */

namespace app\Images\controller;

use app\BaseController;
use think\facade\Db;
use think\facade\Filesystem;
use think\facade\View;

class Images extends BaseController
{
    public function imageHost1()
    {
        $images_id = input('id');
        $tableNameId = input('tableNameId');
        $info = Db::table($tableNameId)
            ->where([
                "frame_id" =>  $images_id,
            ])
            ->find();
        //判断是不是网络文件

        if (!$info) {
            return redirect("https://i0.hdslb.com/bfs/album/45fadaef0640959f78fb165c5a039d891bf5986b.png"); //所访问的图片服务器没有记录
        }

        if (!$info['is_local_file']) { //是否为本地文件
            return redirect("$info[file_server]$info[path]"); //不是本地文件
        } else {
            try { //如果文件不存在会抛出异常
                $file = Filesystem::disk("ProjectImages")->read("$info[path]");
                return response($file, 200)->contentType('image/gif,image/png,image/jpg,image/jpeg');
            } catch (\Throwable $th) { //抛出异常
                return redirect("https://i0.hdslb.com/bfs/album/bd1bde615215612407258a772c4882fc32819c32.png"); //不存在 提示文件已被删除
            }
        }
    }
    public function cover()
    {
        $images_id = input('id');
        $tableNameId = input('tableNameId');
        $info = Db::table("$tableNameId")
            ->where([
                "frame_id" =>  $images_id,
            ])
            ->find();
        //判断是不是网络文件

        if ($info) {
            if ($info["file_server"] == "https://storage1.picacomic.com") {
                $info['path'] = config('GlobalSettings.image_sever_cover') . "$info[file_server]/static/$info[path]";
            }
        } else {
            return redirect("https://i0.hdslb.com/bfs/album/45fadaef0640959f78fb165c5a039d891bf5986b.png"); //所访问的图片服务器没有记录
        }
        // $url = 'public/update/imagesupdate/5af393a7db91d02c83d987b4/582186a35f6b9a4f93dcb549/582186a35f6b9a4f93dcb54b/81832154_p0.jpg';
        //file_get_contents($url,true); 可以读取远程图片，也可以读取本地图片

        //判断是否是 url

        if (is_url($info['path'])) { //是否为 url
            return redirect($info['path']); //不存在 则打开文件链接
        } else {
            $file = Filesystem::disk("ProjectImages")->read($info['path']);
            if ($file) {
                return response($file, 200)->contentType('image/gif,image/png,image/jpg,image/jpeg');
            } else {
                return redirect("https://i0.hdslb.com/bfs/album/bd1bde615215612407258a772c4882fc32819c32.png"); //不存在 提示文件已被删除
            }
        }
    }
}
function is_url($v)
{
    $pattern = "#(http|https)://(.*\.)?.*\..*#i";
    if (preg_match($pattern, $v)) {
        return true;
    } else {
        return false;
    }
}
