<?php
/*
 * @Date: 2020-04-27 17:14:15
 * @名称: 图片类
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-31 20:07:32
 * @FilePath: /服务器/app/Images/route/route.php
 */

use think\facade\Route;

Route::rule('static/:tableNameId/:id', 'Images/imageHost1', 'get');
Route::rule('cover/:tableNameId/:id', 'Images/cover', 'get');
