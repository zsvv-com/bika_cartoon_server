<?php
/*
 * @Date: 2020-06-19 11:16:37
 * @名称: api设置
 * @描述: api设置 的 默认参数 - 可修改 (请注意 修改此配置 也请修改app的密钥 否则解析请求参数会失败)
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-10-12 20:36:09
 * @FilePath: /Site/app/api/config/apiconfig.php
 * @最后编辑: 初雪桜
 */

use think\facade\Db;

return [

    'comicsComicsInfoSelectCount' => 50, //阅读漫画一页显示几幅动画在获取下一幅
    'comicsAllChapterSelectCount' => 5, //全部章节 每次查询几个
    'comicsAllClassSelectCount' => 5, //分类列表每次显示多少个
    'commentCommentListSelectCount' => 5, //每次显示几个评论
    'commentSonCommentListSelectCount' => 5, //子评论每次显示几个
    'searchSelectCount' => 20, //搜索漫画每次显示几个
    'sonCommentDataBaseMaxCount' => 20000, //!评论每一个子评论表最大数量
    'bookLikeDataBaseMaxCount' => 2000, //!书籍喜欢表每一张最多可以有多少用户
    'comicsLikeDataBaseMaxCount' => 2000, //!评论表 每张表所容纳的用户

    //*令牌解析 - 非紧急状况禁止修改
    /* 重要
     * 非紧急情况请勿修改 修改完所有登陆信息会失效 
     */
    'TokenResolutionKey' => '÷MGrˆIjzMy40DNd(n˚rzˆ‘¥eTN¥QM¬≈kU÷ΩWVd!Ek0)∂i†)H©%UßIhtœp˜¥4^E˙9ß≥@0sN(®TΩµgcΩkSsD†˚$T75å‘…“jµ(Ωpånˆ9&tUE$∆t©OTIk!´3vc©˚˜S&1@k0YƒI6RR)÷VGM≤œIç40å7M!n√ndttßΩ≈DƒTqm8x…p√9AπLBopHKT√¨2wp)AB9&424JiXßn4nçfçqQ)X7¨©ZSyB&≤q…4N¥HJ˚…7S!œt%å8oTfn¥d5∆myp*O*≥CP˚˜FKπFfj', //*一般情况禁止修改 修改了会所有token验证失败 需要重新登陆获取令牌
    /* 重要
     * 非紧急情况请勿修改 修改完所有登陆信息会失效 
     * 这是用户令牌sha512的密钥 更改玩会验证失败
     */
    'TokenResolutionKeySha512' => 'CƒpM®IQ…å∂ßø#PZƒZcgkœv&B∑eCgxaΩO3iFUvCjBnxY˚≥ß¥®∑FZRp#S#JçUG$Ωgzjß2dhA®…b≥hE˚¥4¨†∑¬÷U)≈‘ΩSoh÷πm*˜eV4¥x)œNAIa†d^)^˚d≥)¨)oKsRS*kA´)Z®qa≤Za7´DO43∑ƒ%QCfqW“QsπˆAj^7$w≤…D07u´cPm0wΩ∫%PHq∂©˜¨vOyƒ0Zfπ¬Xhuøˆ˚b‘≤D8≥X´pdNh*∂Ov5d©v¨ZC¬∂J*eß≥r1y%π3´Af¬No2e)9tKs*$*!0kru', //*一般情况禁止修改 修改了会所有token验证失败 需要重新登陆获取令牌

    // 默认使用的数据库连接配置
    'RequestToVerifyEncryptionKey' => 'kKMTTGDj5ab9wBQA2lnx8qCSI4JInc9sVq825CDlzXj8NQEvkrhJr4thEJLhQ67JCBkOBT39TyJfICunTh3zy0GRBAbt7uRTgjPIltbEiyY8XmYhEi1EpEQbrMKGIL8L', //请求参数验证密钥
    // 请求时间超时 超过多少秒不进行处理
    'OpenOrNotRequestTimeOut' => false, //是否开启 超时验证 bool
    'RequestTimeOut' => 180, //请求链接超过180秒 也就是3分钟不进行处理

    //!标识类
    "UserIdentification" => 'user',
    "LikeIdentification" => 'like',
    "UserViewHistory" => 'ViewHistory',
    "Comment" => 'comment',
    "SonComment" => 'son-comment',
    "CommentLike" => 'comment-like',
    //!标识类
    //?查询类
    "UserFavouriteSelectNumber" => 20, //用户喜欢数量每次查询条数
    //?查询类
];
