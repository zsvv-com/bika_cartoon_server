<?php
/*
* @Date: 2020-07-06 23:32:36
* @名称: 语言文件
* @描述: 英语
* @版本: 0.01
* @作者: 初雪桜
* @邮箱: 202184199@qq.com
* @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-07 20:26:45
 * @FilePath: /Pica_acg服务器/app/api/config/lanEnUs.php
* @最后编辑: 初雪桜
*/
return [
    //token
    "T-10000" => "Invalid token", //无效的令牌
    "T-10001" => "User token has an unexpected error Please log in again", //用户令牌产生意外错误 请重新登录
    "T-10002" => "token expired please login again", //token 过期 请重新登录
    //token
    //*APP类
    "A-10000" => "Application verification failed", //应用程序验证失败
    "A-10001" => "App needs to be updated", //应用需要更新
    //*APP类
    //?用户类
    "U-10001" => "User authentication failed", //用户验证失败
    "U-10001" => "Username or password cannot be empty", //用户名或密码不能为空
    "U-10002" => "Invalid username or password", //无效的用户名或密码
    //?用户类
    //!其他类
    //*App类
    "Other-A*00001" => "App application error The current version needs to be updated mandatory minimum recommended version", //app应用程序错误 当前版本需要强制更新 最低推荐版本
    "Other-A*00002" => "App application error The current version needs to be updated to the latest version", //app应用程序错误 当前版本需要强制更新 最新版本
    "Other-A*00003" => "app application error Failed to get the latest application", //app应用程序错误 获取最新应用程序失败
    "Other-A*00004" => "App application error The current application does not exist, you can download the new version", //app应用程序错误 当前应用程序不存在 您可以下载 新版本
    "Other-A*00005" => "app application error The current version has been disabled Reason for disabling", //app应用程序错误 当前版本已被禁用 禁用原因
    "Other-A*00006" => "Minimum recommended version", //最低推荐版本
    "Other-A*00007" => "The latest version", //最新版本
    "Other-A*00008" => "Minimum recommended version", //最低推荐版本
    //*App类
    //?用户类
    "Undefined-10001" => "unknown error", //未知错误
    //?用户类
    //!其他类
];
