<?php

namespace app\api\controller;

use app\api\controller\Home\Get\GetHomeController;
use app\api\controller\Home\Get\Method\MethodHomeGetPopularClassification;
use app\BaseController;
use think\facade\Db;

class Home extends BaseController
{
    public function Index()
    {
        //请求参数验证
        $RequestAuthenticationAndUserAuthentication = RequestAuthenticationAndUserAuthentication(true, true, true, true, config('debug.debug'));
        if ($RequestAuthenticationAndUserAuthentication['code'] != 200) { //判断是否都验证通过了
            #没有验证通过
            return json($RequestAuthenticationAndUserAuthentication);
        }
        //请求参数验证
        $sources_data[0]["isPunched"] = false;
        $sources_data[0]["latestApplication"] = null;
        $sources_data[0]["imageServer"] = "http://192.168.1.105//static/";
        $data["code"] = "200";
        $data["message"] = "success";
        $sources["sources"] = [];
        foreach ($sources_data as $key => $value) {
            array_push($sources["sources"], $value);
        }
        $data["data"] = $sources;
        sleep(1);
        return json($data);
    }
    /**
     * 获取横幅
     *
     * @return void
     */
    public function Banner()
    {
        $GetHomeController = new GetHomeController($this->app);
        return $GetHomeController->HomeGetBanner();
    }
    /**
     * 获取公告
     *
     * @return void
     */
    public function announcements()
    {
        $GetHomeController = new GetHomeController($this->app);
        return $GetHomeController->HomeGetAnnouncements();
    }

    public function CategoryEveryoneSearch()
    {
        //请求参数验证
        $RequestAuthenticationAndUserAuthentication = RequestAuthenticationAndUserAuthentication(true, true, true, true, config('debug.debug'));
        if ($RequestAuthenticationAndUserAuthentication['code'] != 200) { //判断是否都验证通过了
            #没有验证通过
            return json($RequestAuthenticationAndUserAuthentication);
        }
        //请求参数验证


        $data["code"] =   "200";
        $data["message"] = "success";


        $data['data']["keywords"] = array();
        $data['data']["keywords"][0] = 'C96';
        $data['data']["keywords"][1] = '嗶咔團長推薦';
        $data['data']["keywords"][2] = '肥宅';
        $data['data']["keywords"][3] = '老師';
        $data['data']["keywords"][4] = '校園';
        $data['data']["keywords"][5] = '校服';
        $data['data']["keywords"][6] = '廁所';
        $data['data']["keywords"][7] = '水平線';
        $data['data']["keywords"][8] = '冰菓';
        $data['data']["keywords"][9] = '一拳超人';
        $data['data']["keywords"][10] = '遊戲王';
        $data['data']["keywords"][11] = '小梅けいと';
        $data['data']["keywords"][12] = '40010';
        $data['data']["keywords"][13] = 'ホムンクルス';

        return json($data);
    }
    /**
     * 获取全部分类
     *
     * @return void
     */
    function PopularClassification()
    {
        $GetHomeController = new GetHomeController($this->app);
        return $GetHomeController->HomeGetPopularClassification();
    }
}
