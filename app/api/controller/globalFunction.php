<?php
/*
 * @Date: 2020-03-20 19:42:23
 * @名称: 全局方法
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-06 22:55:55
 * @FilePath: /Pica_acg服务器/app/api/controller/globalFunction.php
 */



namespace app\api\controller;

use app\BaseController;
use think\facade\Db;

class GlobalFunction extends BaseController
{

    /**
     * 生成随机id
     *
     * @param Int $randType 数据类型 0 喜欢的id
     * @param Int $length 长度 默认22
     * @return String 数据
     */
    public function randIdStructure($randType, $length = 22)
    {

        function str_rand($length, $char = '0123456789abcdefghijklmnopqrstuvwxyz')
        {
            if (!is_int($length) || $length < 0) {
                return false;
            }

            $string = '';
            for ($i = $length; $i > 0; $i--) {
                $string .= $char[mt_rand(0, strlen($char) - 1)];
            }

            return $string;
        }
        $stateString = null;
        switch ($randType) {
            case 0: // 喜欢id
                $stateString = "like";
                break;
            default:
                # code...
                break;
        }
        $stateString .= str_rand($length - strlen($stateString));
        return $stateString;
    }
}
