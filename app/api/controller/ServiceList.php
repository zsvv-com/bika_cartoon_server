<?php
/*
 * @Author: your name
 * @Date: 2019-11-26 09:32:10
 * @LastEditTime: 2020-10-12 22:08:29
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /Site/app/api/controller/ServiceList.php
 */

namespace app\api\controller;

use app\BaseController;

class ServiceList extends BaseController
{
    public function index()
    {
        $RequestAuthenticationAndUserAuthentication = RequestAuthenticationAndUserAuthentication(true, true, true, true, config('debug.debug'));
        if ($RequestAuthenticationAndUserAuthentication['code'] != 200) { //判断是否都验证通过了
            #没有验证通过
            return json($RequestAuthenticationAndUserAuthentication);
        }
        $sources_data[1]["title"] =  "冷火服務器";
        $sources_data[1]["code"] =  "CG1";
        $sources_data[1]["url"] =  "http://okabe.natapp1.cc/api/Index";
        $sources_data[1]["chat"] =  "http://okabe.natapp1.cc/api/Index";

        $sources_data[2]["title"] =  "原點服務器";
        $sources_data[2]["code"] =  "CG2";
        $sources_data[2]["url"] =  "http://okabe.natapp1.cc/api/Index";
        $sources_data[2]["chat"] =  "http://okabe.natapp1.cc/api/Index";

        $sources_data[3]["title"] =  "暗夜服務器";
        $sources_data[3]["code"] =  "CG3";
        $sources_data[3]["url"] =  "http://okabe.natapp1.cc/api/Index";
        $sources_data[3]["chat"] =  "http://okabe.natapp1.cc/api/Index";

        $sources_data[4]["title"] =  "魔神服務器";
        $sources_data[4]["code"] =  "CG4";
        $sources_data[4]["url"] =  "http://okabe.natapp1.cc/api/Index";
        $sources_data[4]["chat"] =  "http://okabe.natapp1.cc/api/Index";

        $sources_data[5]["title"] =  "神光服務器";
        $sources_data[5]["code"] =  "CG5";
        $sources_data[5]["url"] =  "http://okabe.natapp1.cc/api/Index";
        $sources_data[5]["chat"] =  "http://okabe.natapp1.cc/api/Index";
        $data["code"] = "200";
        $data["message"] = "success";
        $sources["sources"] = [];
        foreach ($sources_data as $key => $value) {
            array_push($sources["sources"], $value);
        }
        $data["data"] = $sources;
        sleep(1);
        return json($data);
    }
}
