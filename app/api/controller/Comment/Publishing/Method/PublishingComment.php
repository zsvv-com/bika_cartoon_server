<?php
/*
 * @Date: 2020-03-20 19:42:23
 * @名称: 发布评论
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-09-10 22:13:24
 * @FilePath: /Site/app/api/controller/Comment/Publishing/Method/PublishingComment.php
 */

namespace app\api\controller\Comment\Publishing\Method;

use app\api\model\BookList;
use app\BaseController;
use think\facade\Db;

class PublishingComment extends BaseController
{
    /**
     * 发布评论
     *
     * @param [String] $book_id 书籍名
     * @param [String] $content 评论的内容
     * @param [Map] $user_info 用户信息
     * @param [String] $subsection_id 章节id 可为空
     * @return void
     */
    public function PublishingComment($book_id, $content,  $user_info, $subsection_id = null)
    {
        $commentTableName = (Db::table('book_list')
            ->where([
                "_id" => $book_id,
            ])
            ->field(["comment_table_name"])
            ->find());
        $sonTableName = (Db::table('setting')
            ->where([
                "id" => 1
            ])
            ->field(["son_comment_table_name"])
            ->find());
        if (!$commentTableName || !$commentTableName["comment_table_name"]) {
            return errorJsonReturn("Book-10001");
        }
        $commentTableName = $commentTableName["comment_table_name"]; //查询出来的
        $sonTableName = $sonTableName["son_comment_table_name"]; //查询出来的
        try {
            Db::table($commentTableName)
                ->insert([
                    "user_id" => $user_info["_id"],
                    "comment_id" => randIdStructure(3, 120), //评论的id
                    "book_id" => $book_id, //书籍id
                    "subsection_id" => $subsection_id, //分卷id
                    "content" => $content, //内容
                    "is_top" => '0', //是否置顶
                    "hide" => '0', //是否隐藏
                    "created_at" => date(DATE_ISO8601, time()), //发布时间
                    "likes_count" => '0', //喜欢人数
                    "comments_count" => '0', //评论人数
                    "son_comment_table_name" => $sonTableName,
                ]);
        } catch (\Throwable $th) {
            return errorJsonReturn("Comment-Main-10001");
        }
        return successJsonReturn();
    }
}
