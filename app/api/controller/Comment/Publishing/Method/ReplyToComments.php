<?php
/*
 * @Date: 2020-03-20 19:42:23
 * @名称: 发布评论
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-09-10 23:24:34
 * @FilePath: /Site/app/api/controller/Comment/Publishing/Method/ReplyToComments.php
 */

namespace app\api\controller\Comment\Publishing\Method;

use app\api\model\BookList;
use app\BaseController;
use think\facade\Db;

class ReplyToComments extends BaseController
{
    /**
     * 回复评论
     *
     * @param String $book_id //@a 书籍id 
     * @param String $content //@a 评论的内容
     * @param Map $user_info //@a 用户信息
     * @param String $_comment_id //@a 评论id
     * @param String $_comment_table_name //@a 评论所在的表的名字
     * @return void
     */
    public function ReplyToComments($book_id, $content,  $user_info, $_comment_id, $_comment_table_name)
    {
        if (
            !$content || !$_comment_id || !$_comment_table_name
        ) {
            return errorJsonReturn("Book-10001");
        }

        try {
            $coment_info = Db::table($_comment_table_name)
                ->where([
                    "comment_id" => $_comment_id,
                    "book_id" => $book_id,
                ])
                ->field([
                    "son_comment_table_name", //字表名称
                    "subsection_id", //分卷id
                ])
                ->find();
        } catch (\Throwable $th) {
            return errorJsonReturn("Comment-Main-10001");
        }
        if (!$coment_info) {
            return errorJsonReturn("Comment-Main-10003");
        }
        $son_table_name = $coment_info['son_comment_table_name'];
        $subsection_id = $coment_info['subsection_id'];
        $CurrentTableCount = Db::table($son_table_name)->count();
        $son_comment_table_name = $son_table_name; //!当前评论的子评论所在表
        if ($CurrentTableCount <= config("apiconfig.sonCommentDataBaseMaxCount")) {
            $sonTableStart = Db::table("book_comment_son_controller")
                ->where([
                    "table_name" => $son_table_name
                ])
                ->field([
                    "count_max"
                ])
                ->find(); //!查询 当前表 是否已经满数据 如果没满则会设置为满了 如果满了 则 查询最新的表
            if ($sonTableStart["count_max"] == 0) { //!如果我查询到我是第一个满了的则
                Db::table("book_comment_son_controller")
                    ->where([
                        "table_name" => $son_table_name
                    ])
                    ->update([
                        "count_max" => 1,

                    ]);
                $randStr = randIdStructure(999999999, 32);
                Db::table("book_comment_son_controller")
                    ->insert([
                        "table_name" => "book_comment_son_$randStr",
                        "count_max" => 0,

                    ]);
                $sql = "CREATE TABLE `book_comment_son_$randStr` (
                    `id` int NOT NULL AUTO_INCREMENT,
                    `user_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户id',
                    `comment_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '评论id随机生成',
                    `book_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '漫画id',
                    `subsection_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '漫画分卷id',
                    `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin COMMENT '评论内容',
                    `is_top` tinyint(1) DEFAULT NULL COMMENT '是否置顶 0和1',
                    `hide` tinyint(1) DEFAULT NULL COMMENT '是否隐藏 0和1',
                    `created_at` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '发布时间',
                    `likes_count` int DEFAULT NULL COMMENT '喜欢数量',
                    `comments_count` int DEFAULT NULL COMMENT '评论数量',
                    `father_comment_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '父评论id',
                    `son_comment_table_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '字评论表名',
                    `father_commment_table_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '父评论所在表',
                    PRIMARY KEY (`id`) USING BTREE
                  ) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='字评论 和楼中楼评论到了 一定数据 自动加一';
                  "; //! 创建 子评论表的sql语句
                Db::query($sql);
                $son_comment_table_name = "book_comment_son_$randStr";
            } else {
                $son_comment_table_name  = Db::table("book_comment_son_controller")
                    ->order('id desc')
                    ->find()['table_name']; //!查询 当前表 是否已经满数据 如果没满则会设置为满了 如果满了 则 查询最新的表

            }
        }
        Db::table($son_table_name)
            ->insert([
                "user_id" => $user_info["_id"],
                "comment_id" => randIdStructure(4, 120),
                "book_id" => $book_id, //书籍id
                "subsection_id" => $subsection_id,
                "content" => $content, //内容
                "is_top" => "0", //是否置顶
                "hide" => "0", //是否隐藏
                "created_at" => date(DATE_ISO8601, time()), //发布时间
                "likes_count" => "0", //喜欢总数
                "comments_count" => "0", //评论总数
                "father_comment_id" => $_comment_id, //父及评论 id
                "son_comment_table_name" => $son_comment_table_name,
                "father_commment_table_name" => $_comment_table_name,
            ]);
    }
}
