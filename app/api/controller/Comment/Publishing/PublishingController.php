<?php
/*
 * @Date: 2020-03-20 19:42:23
 * @名称: 全局方法
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-09-10 22:13:45
 * @FilePath: /Site/app/api/controller/Comment/Publishing/PublishingController.php
 */

namespace app\api\controller\Comment\Publishing;

use app\api\controller\Comment\Publishing\Method\PublishingComment;
use app\api\controller\Comment\Publishing\Method\ReplyToComments;
use app\BaseController;
use think\facade\Db;

class PublishingController extends BaseController
{
    public function Publishing($_book_id, $content,  $user_info, $subsection_id = null) //发布评论
    {
        $PublishingComment = new PublishingComment($this->app);
        return $PublishingComment->PublishingComment($_book_id, $content,  $user_info, $subsection_id);
    }
    public function Reply(
        $_book_id, //书籍id
        $content, //我的评论
        $user_info, //我的用户信息
        $_comment_id, //主评论id
        $_comment_table_name //主评论所在数据库
    ) //发布评论
    {
        $ReplyToComments = new ReplyToComments($this->app);
        return $ReplyToComments->ReplyToComments($_book_id, $content,  $user_info, $_comment_id, $_comment_table_name);
    }
}
