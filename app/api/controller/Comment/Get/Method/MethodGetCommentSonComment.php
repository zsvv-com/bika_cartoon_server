<?php
/*
 * @Date: 2020-03-20 19:42:23
 * @名称: 获取 评论的子评论
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-10-12 21:02:05
 * @FilePath: /Site/app/api/controller/Comment/Get/Method/MethodGetCommentSonComment.php
 */

namespace app\api\controller\Comment\Get\Method;

use app\BaseController;
use think\facade\Db;

class MethodGetCommentSonComment extends BaseController
{
    /**
     * 获取子评论
     *
     * @param String $page //@a 第几页
     * @param String $comment_id //@a 获取那个评论额id
     * @param String $book_id //@a 那一本书籍的
     * @param String $comment_table_name //@a 这个评论所在的表
     * @return void
     */
    public function StartGet($page, $comment_id, $book_id, $comment_table_name)
    {
        $page = (int)input('get.page', 1);
        $comment_id = input('get.comment_id'); //@a 评论的id
        $book_id = input('post.book_id');
        $comment_table_name = input('post.comment_table_name'); //@v 新参数 主评论表的名字
        //通过查询父评论 表中的 son_comment_table_name 来查找字评论所在的表



        $data["message"] = "success";
        $data["data"]["comments"]["page"] = "1";
        $data["code"] = "200";
        $data["data"]["comments"]["pages"] = 1;
        $data["data"]["comments"]["docs"] = [];
        try {
            $son_comment_list = Db::table($comment_table_name)
                ->where([
                    "father_comment_id" => $comment_id,
                    "book_id" => $book_id,
                ])
                ->alias("son_table_name")
                ->join('user_info user_info', "user_info._id = son_table_name.user_id")
                ->order([
                    "son_table_name.is_top" => "desc",
                    "son_table_name.id" => "desc",
                ])
                ->limit($page * config("apiconfig.commentSonCommentListSelectCount") - config("apiconfig.commentSonCommentListSelectCount"), config("apiconfig.commentSonCommentListSelectCount"))
                ->field([
                    //! 用户类
                    "user_info._id as userInfoId", //发布者id
                    "user_info.gender as userInfoGender", //性别 0机器人 1男性 2 女性
                    "user_info.name as userInfoName", //发布者 昵称
                    "user_info.slogan as userInfoSlogan", //发布者 个性签名
                    "user_info.verified as userInfoVerified", //认证用户 真假
                    "user_info.exp as userInfoExp", //经验
                    // "user_info.title as userInfoTitle", //发布者 等级名称 类似于冒泡潜水之类的
                    // "user_info.level as userInfolevel", //等级
                    "user_info.characters as userInfoCharacters", //不知道干什么的
                    "user_info.file_server as userInfoFileServer", //头像服务器
                    "user_info.path as userInfoPath", //头像路径
                    "user_info.original_name as userInfoOriginalName", //保存的名字
                    "user_info.character as userInfoCharacter", //头像框
                    //!子评论类
                    "son_table_name.subsection_id as bookComment_id", //分卷id
                    "son_table_name.content as bookCommentContent", //评论内容
                    "son_table_name.book_id as bookComment_comic", //漫画id
                    "son_table_name.is_top as bookCommentIsTop", //是否置顶
                    "son_table_name.hide as bookCommentHide", //是否隐藏
                    "son_table_name.created_at as bookCommentCreated_at", //发布时间
                    "son_table_name.father_comment_id as fatherCommentId", //父级id
                    "son_table_name.comment_id as CommentId", //自己评论的id
                    "son_table_name.likes_count as bookCommentLikesCount", //评论id
                    "son_table_name.son_comment_table_name as sonConmentTableName", //子评论所在表
                ])
                ->select();
        } catch (\Throwable $th) {
            dump($th);
            return errorJsonReturn("Server-10000", "错误位置 获取字评论 错误行" . __LINE__);
        }

        $data_list = [];
        foreach ($son_comment_list as $key => $value) {
            $level_info = Db::table('user_level')
                ->where([
                    ["min_experience", "<=", $value['userInfoExp']],
                    ["max_experience", ">=", $value['userInfoExp']]
                ])
                ->field([
                    "level_name as userInfoTitle", //等级名
                    "level as userInfolevel" //等级
                ])
                ->find();

            switch ($value["userInfoGender"]) {
                    //请注意这里 如果 这是是bot 是没有 下面这三个的
                case 0:
                    $gender = "机器人";
                    break;
                case 1:
                    $gender = "男性";
                    break;
                case 2:
                    $gender = "女性";
                    break;
                default:
                    $gender = "UMA";
                    break;
            }

            $data_list[] = [
                "_id" => $value["bookComment_id"], //分卷id
                "content" => $value["bookCommentContent"], //评论内容
                "_comic" => $value["bookComment_comic"], //漫画id
                "isTop" => ($value["bookCommentIsTop"]) ? true : false, //是否置顶
                "hide" => ($value["bookCommentHide"]) ? true : false, //隐藏 - 在官方软件没有效果
                "created_at" => $value["bookCommentCreated_at"], //发布时间
                "_user" => [
                    "characters" => ['aaa'], //关键字,
                    "exp" => $value["userInfoExp"], //经验
                    "slogan" => $value["userInfoSlogan"], //个性签名
                    "verified" => ($value["userInfoVerified"]) ? true : false,
                    "_id" => $value["userInfoId"],
                    "title" => $level_info["userInfoTitle"],
                    "level" => $level_info["userInfolevel"],
                    "avatar" => [
                        "path" => $value["userInfoPath"],
                        "originalName" => $value["userInfoOriginalName"],
                        "fileServer" => $value["userInfoFileServer"],
                    ],
                    "role" => "member", //不知道是啥
                    "character" => $value["userInfoCharacter"],
                    "gender" => $gender,
                    "name" => $value["userInfoName"],
                ],
                "_parent" => $value["fatherCommentId"], //父级id
                "comment_id" => $value["CommentId"], //评论id
                "son_table_name" => $value['sonConmentTableName'], //子评论所在表
                "likesCount" => $value["bookCommentLikesCount"],
                "commentCount" => Db::table($value["sonConmentTableName"])->count(),
                "isLiked" => false, //自己是否喜欢
            ];
        }
        $count = Db::table($comment_table_name)
            ->where([
                "father_comment_id" => $comment_id,
                "book_id" => $book_id,
            ])->count();
        $data["data"]["comments"]["docs"] = $data_list;
        $data["data"]["comments"]["total"] = $count;
        $data["data"]["comments"]["limit"] = config("apiconfig.commentSonCommentListSelectCount");
        $data["data"]["comments"]["page"] = $page + 1;
        $data["data"]["comments"]["pages"] = ceil($count / config("apiconfig.commentSonCommentListSelectCount"));

        return json($data);
    }
}
