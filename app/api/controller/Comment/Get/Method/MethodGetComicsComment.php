<?php
/*
 * @Date: 2020-03-20 19:42:23
 * @名称: 获取 漫画评论
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-10-12 22:05:29
 * @FilePath: /Site/app/api/controller/Comment/Get/Method/MethodGetComicsComment.php
 */

namespace app\api\controller\Comment\Get\Method;

use app\BaseController;
use think\facade\Db;

class MethodGetComicsComment extends BaseController
{
    /**
     * 开始获取漫画评论
     *
     * @param String $bookId //@a 漫画id
     * @param String $subsectionId //@a 分卷id
     * @param Int $page //@a 第几页
     * @return void
     */
    public function StartGet($bookId, $subsectionId, $page)
    {
        $RequestAuthenticationAndUserAuthentication = RequestAuthenticationAndUserAuthentication(true, true, true, true, config('debug.debug'));
        if ($RequestAuthenticationAndUserAuthentication['code'] != 200) { //判断是否都验证通过了
            #没有验证通过
            return json($RequestAuthenticationAndUserAuthentication);
        }
        $selectUserInfo = $RequestAuthenticationAndUserAuthentication['data']['userVersion']['data'];
        $comment_table_name = Db::table("book_list") //查询漫画的评论所在的表
            ->where([
                "_id" => $bookId,
            ])
            ->field('comment_table_name')
            ->find();

        if (!$comment_table_name) {
            return errorJsonReturn("Book-10000");
        }
        $comment_table_name = $comment_table_name["comment_table_name"];

        if ($subsectionId && $subsectionId != null && $subsectionId != "null") {
            $commentSelectWhere = [
                "book_id" => $bookId,
                "subsection_id" => $subsectionId,
            ];
        } else {
            $commentSelectWhere = [
                "book_id" => $bookId,
            ];
        }


        $allComment = Db::table($comment_table_name)
            ->where($commentSelectWhere)
            ->alias('book_comment')
            ->join('user_info user_info', 'user_info._id = book_comment.user_id')
            ->count();
        $data['message'] = "success";
        $data['data']["comments"]["page"] = "$page";
        $data['data']["comments"]["pages"] = ceil($allComment / config("apiconfig.commentCommentListSelectCount"));
        $data['data']["comments"]["docs"] = [];
        $data['data']["comments"]["total"] = $allComment;
        $data['data']["comments"]["limit"] = 20;
        $data['code'] = "200";
        $commentList = Db::table($comment_table_name)
            ->where($commentSelectWhere)
            ->alias('book_comment')
            ->join('user_info user_info', 'user_info._id = book_comment.user_id')
            ->order([
                "book_comment.is_top" => "desc",
                "book_comment.id" => "desc",
            ])
            ->limit($page * config("apiconfig.commentCommentListSelectCount") - config("apiconfig.commentCommentListSelectCount"), config("apiconfig.commentCommentListSelectCount"))
            ->field([
                //! 用户类
                "user_info._id as userInfoId", //发布者id
                "user_info.gender as userInfoGender", //性别 0机器人 1男性 2 女性
                "user_info.name as userInfoName", //发布者 昵称
                "user_info.slogan as userInfoSlogan", //发布者 个性签名
                "user_info.verified as userInfoVerified", //认证用户 真假
                "user_info.exp as userInfoExp", //经验
                // "user_info.title as userInfoTitle", //发布者 等级名称 类似于冒泡潜水之类的
                // "user_info.level as userInfolevel", //等级
                "user_info.characters as userInfoCharacters", //不知道干什么的
                "user_info.file_server as userInfoFileServer", //头像服务器
                "user_info.path as userInfoPath", //头像路径
                "user_info.original_name as userInfoOriginalName", //保存的名字
                "user_info.character as userInfoCharacter", //头像框
                //!评论类
                "book_comment.subsection_id as bookComment_id", //分卷id
                "book_comment.content as bookCommentContent", //评论内容
                "book_comment.book_id as bookComment_comic", //漫画id
                "book_comment.is_top as bookCommentIsTop", //是否置顶
                "book_comment.hide as bookCommentHide", //是否隐藏
                "book_comment.created_at as bookCommentCreated_at", //发布时间
                "book_comment.comment_id as bookCommentId", //评论id
                "book_comment.son_comment_table_name as son_comment_table_name", //字评论所在表
                "book_comment.likes_count as bookCommentLikesCount", //这个评论共有多少人喜欢
            ])
            ->select();

        $data_list = []; //评论
        foreach ($commentList as $key => $value) {

            // 查询等级
            $level_info = Db::table('user_level')
                ->where([
                    ["min_experience", "<=", $value['userInfoExp']],
                    ["max_experience", ">=", $value['userInfoExp']]
                ])
                ->field([
                    "level_name as userInfoTitle", //等级名
                    "level as userInfolevel" //等级
                ])
                ->find();

            $LikeState =  Db::table("$selectUserInfo[commentLikeTable]") //@a 定位到喜欢表 从喜欢表中查询数据
                ->where([
                    "user_id" => $selectUserInfo['_id'],
                    "comment_id" => $value['bookCommentId'],
                ])
                ->find();

            switch ($value["userInfoGender"]) {
                    //请注意这里 如果 这是是bot 是没有 下面这三个的
                case 0:
                    $gender = "机器人";
                    break;
                case 1:
                    $gender = "男性";
                    break;
                case 2:
                    $gender = "女性";
                    break;

                default:
                    $gender = "量子";
                    break;
            }

            $data_list[]  = [
                "_id" => $value["bookComment_id"], //分卷id
                "content" => $value["bookCommentContent"], //评论内容
                "_comic" => $value["bookComment_comic"], //漫画id
                "isTop" => ($value["bookCommentIsTop"]) ? true : false, //是否置顶
                "hide" => ($value["bookCommentHide"]) ? true : false, //隐藏 - 在官方软件没有效果
                "created_at" => $value["bookCommentCreated_at"], //发布时间
                "_user" => [
                    "characters" => ['aaa', '用户关键字'], //不知道是什么 
                    "exp" => $value["userInfoExp"], //经验
                    "slogan" => $value["userInfoSlogan"], //个性签名
                    "verified" => ($value["userInfoVerified"]) ? true : false,
                    "_id" => $value["userInfoId"],
                    "title" => $level_info["userInfoTitle"],
                    "level" => $level_info["userInfolevel"],
                    "avatar" => [
                        "path" => $value["userInfoPath"],
                        "originalName" => $value["userInfoOriginalName"],
                        "fileServer" => $value["userInfoFileServer"],
                    ],
                    "role" => "member", //不知道是啥
                    "character" => $value["userInfoCharacter"],
                    "gender" => $gender, //!性别
                    "name" => $value["userInfoName"],
                ],
                "id" => $value["bookCommentId"], //评论id
                "table_name" => $comment_table_name,
                "son_table_name" => $value['son_comment_table_name'], //子评论所在表
                "likesCount" => $value["bookCommentLikesCount"],
                "commentsCount" => Db::table($value['son_comment_table_name'])->where(["father_comment_id" => $value["bookCommentId"],])->count(), //评论id
                "isLiked" => ($LikeState) ? true : false,
            ];
        }
        foreach ($data_list as $key => $value) {
            array_push($data['data']["comments"]["docs"], $value);
        }

        return json($data);
    }
}
