<?php
/*
 * @Date: 2020-03-20 19:42:23
 * @名称: 获取评论控制器 获取所有评论的
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-09-12 22:56:53
 * @FilePath: /Site/app/api/controller/Comment/Get/GetCommentController.php
 */

namespace app\api\controller\Comment\Get;

use app\api\controller\Comment\Get\Method\MethodGetComicsComment;
use app\api\controller\Comment\Get\Method\MethodGetCommentSonComment;
use app\BaseController;

class GetCommentController extends BaseController
{
    /**
     * 获取漫画评论
     *
     * @param String $bookId //@a 漫画id
     * @param String $subsectionId //@a 分卷id
     * @param Int $page //@a 第几页
     * @return void
     */
    public function GetComicsComment($bookId, $subsectionId, $page)
    {
        $GetComicsComment = new MethodGetComicsComment($this->app);
        return $GetComicsComment->StartGet(
            $bookId, //@a 漫画id
            $subsectionId, //@a 分卷id
            $page //@a 第几页
        );
    }
    /**
     * 获取子评论
     *
     * @param String $page //@a 第几页
     * @param String $comment_id //@a 获取那个评论额id
     * @param String $book_id //@a 那一本书籍的
     * @param String $comment_table_name //@a 这个评论所在的表
     * @return void
     */
    public function GetCommentSonComment($page, $comment_id, $book_id, $comment_table_name)
    {
        $GetComicsComment = new MethodGetCommentSonComment($this->app);
        return $GetComicsComment->StartGet(
            $page, //@a 第几页
            $comment_id, //@a 获取那个评论额id
            $book_id, //@a 那一本书籍的
            $comment_table_name //@a 这个评论所在的表
        );
    }
}
