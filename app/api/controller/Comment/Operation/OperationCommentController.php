<?php
/*
 * @Date: 2020-03-20 19:42:23
 * @名称: 获取评论控制器 操作 (喜欢/不喜欢) \ 举报
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-09-12 23:24:29
 * @FilePath: /Site/app/api/controller/Comment/Operation/OperationCommentController.php
 */

namespace app\api\controller\Comment\Operation;

use app\api\controller\Comment\Get\Method\MethodGetComicsComment;
use app\api\controller\Comment\Get\Method\MethodGetCommentSonComment;
use app\api\controller\Comment\Operation\Method\MethodLikeComment;
use app\BaseController;

class OperationCommentController extends BaseController
{
    /**
     * 喜欢 / 不喜欢 评论
     *
     * @param String $comment_id //@a 评论id
     * @param String $comment_table_name //@a 评论所在表
     * @return void
     */
    public function LikeComment(
        $comment_id, //@a 评论id 
        $comment_table_name //@a 评论所在的表的名称
    ) {
        $MethodLikeComment = new MethodLikeComment($this->app);
        return $MethodLikeComment->Run(
            $comment_id, //@a 评论id 
            $comment_table_name //@a 评论所在的表的名称
        );
    }
}
