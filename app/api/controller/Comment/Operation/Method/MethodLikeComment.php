<?php
/*
 * @Date: 2020-03-20 19:42:23
 * @名称: 评论 - (喜欢/不喜欢)某个评论
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-09-12 23:24:20
 * @FilePath: /Site/app/api/controller/Comment/Operation/Method/MethodLikeComment.php
 */

namespace app\api\controller\Comment\Operation\Method;

use app\BaseController;
use think\facade\Db;

class MethodLikeComment extends BaseController
{
    /**
     * 喜欢 / 不喜欢 评论
     *
     * @param String $comment_id //@a 评论id
     * @param String $comment_table_name //@a 评论所在表
     * @return void
     */
    public function Run(
        $comment_id, //@a 评论id 
        $comment_table_name //@a 评论所在的表的名称
    ) {

        $RequestAuthenticationAndUserAuthentication = RequestAuthenticationAndUserAuthentication(true, true, true, true, config('debug.debug'));
        if ($RequestAuthenticationAndUserAuthentication['code'] != 200) { //判断是否都验证通过了
            #没有验证通过
            return json($RequestAuthenticationAndUserAuthentication);
        }
        //! 查询评论是否存在
        $comment_info = Db::table($comment_table_name)
            ->where([
                "comment_id" => $comment_id,
            ])
            ->find();
        if (!$comment_info) {
            return errorJsonReturn("Comment-Main-10000");
        }

        $user_info = $RequestAuthenticationAndUserAuthentication["data"]['userVersion']['data']; //用户id
        $commentLikeState = Db::table($user_info['commentLikeTable']) //评论点赞状态 可直接判断
            ->where([
                "comment_id" => $comment_id,
                "user_id" => $user_info['_id'],
            ])
            ->find();

        if ($commentLikeState) {
            //喜欢的话 设置为不喜欢
            $deleteState = Db::table($user_info['commentLikeTable']) //删除状态
                ->where([
                    "comment_id" => $comment_id,
                    "user_id" => $user_info['_id'],
                ])
                ->delete();
            Db::table($comment_table_name)
                ->where($comment_info)
                ->dec("likes_count")
                ->update();
            if ($deleteState) {
                $return = ["code" => 200, "message" => "success", "data" => ["action" => "unlike"]];
            } else {
                $return = ["code" => 02, "message" => "error:Cancel like failure/取消喜欢失败"];
            }
        } else {
            //没有数据的时候 设置为喜欢
            $insertState = Db::table($user_info['commentLikeTable'])
                ->insert([
                    "comment_id" => $comment_id,
                    "user_id" => $user_info['_id'],
                    "comment_table_name" => $comment_table_name,
                    "like_id" => randIdStructure(5),
                ]);
            Db::table($comment_table_name)
                ->where($comment_info)
                ->inc("likes_count")
                ->update();
            if ($insertState) {
                $return = ["code" => 200, "message" => "success", "data" => ["action" => "like"]];
            } else {
                $return = ["code" => 03, "message" => "error:Setting like failed/设置喜欢失败"];
            }
        }

        return json($return);
    }
}
