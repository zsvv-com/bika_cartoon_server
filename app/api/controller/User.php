<?php
/*
 * @Date: 2020-01-17 21:42:38
 * @名称: 
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑: 初雪桜
 * @LastEditTime: 2020-09-14 21:42:01
 * @FilePath: /Site/app/api/controller/User.php
 */

namespace app\api\controller;

use app\api\controller\User\Get\GetUserController;
use app\api\controller\User\Operation\OperationUserController;
use app\api\model\BookInfo;
use app\api\model\BookList;
use app\api\model\UserFavourite;
use app\api\model\UserInfo;
use app\BaseController;
use Exception;
use PHPMailer\PHPMailer\PHPMailer;
use think\facade\Db;
use think\facade\Request;

class User extends BaseController
{
    public function Login()
    {
        $userName = input("post.email");
        $passWord = input("post.password");
        $OperationUserController = new OperationUserController($this->app);
        return $OperationUserController->LoginUser($userName, $passWord);
    }
    public function Profile() //基本信息
    {
        $GetUserController = new GetUserController($this->app);
        return $GetUserController->GetUserInfo();
    }
    public function favourite() //我的收藏
    {
        $GetUserController = new GetUserController($this->app);
        return $GetUserController->GetFavourite();
    }
    public function RegUser()
    {
        $userName = ClearHtml(input("post.user_name", null)); //用户名
        $name = ClearHtml(input("post.name", null)); //昵称
        $passWord = input("post.pass_word", null); //密码
        $gender = input("post.gender", 0); //性别

        $OperationUserController = new OperationUserController($this->app);
        return $OperationUserController->RegUser($userName, $name, $passWord, $gender);
    }

    public function ActivateUser() //激活用户
    {
        $AuthcodeStr = input('get.key1');
        $AuthorizationVerification = input('get.key2');
        $OperationUserController = new OperationUserController($this->app);
        return $OperationUserController->ActivateUser($AuthcodeStr, $AuthorizationVerification);
    }
    public function RetrievePassword() //找回密码
    {
        $userName = ClearHtml(input("post.user_name", null)); //用户名 一般是邮箱
        $NewPassWord = input('post.pass_word');
        $OperationUserController = new OperationUserController($this->app);
        return $OperationUserController->RetrievePassword($userName, $NewPassWord);
    }
    public function ResetPassword() //重置密码的 设置新密码
    {
        $AuthcodeStr = input('get.key1');
        $Sha256 = explode('/', input('get.key2'))[0]; //AuthcodeStr 取 Authcode()的 encode 密钥为 $key3 
        $OperationUserController = new OperationUserController($this->app);
        return $OperationUserController->ActivateNewPassword($AuthcodeStr, $Sha256);
    }
}
