<?php
/*
 * @Date: 2020-09-12 23:36:27
 * @名称: 漫画 - 喜欢漫画
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑: 初雪桜
 * @LastEditTime: 2020-09-13 20:07:52
 * @FilePath: /Site/app/api/controller/Comics/Operation/Method/MethodLikeComics.php
 */

namespace app\api\controller\Comics\Operation\Method;

use app\BaseController;
use think\facade\Db;
use think\facade\Request;

class MethodLikeComics extends BaseController
{
    public function StartLikeComics($book_id)
    {
        /**
         * 状态吗 00 没有找到用户 或者有重复的token
         * 状态吗 01 没有传入token
         * 状态吗 02 删除喜欢的数据失败 位于 $deleteState = Db::table('book_info_user_like') //删除状态
         * 状态吗 03 设置喜欢失败插入数据库失败  位于 //没有数据的时候 设置为喜欢
         */
        $info = Request::instance()->header();
        if (!$book_id) {
            $return =  ["code" => "10006", "msg" => "丢失信息"];
        } else {
            if (array_key_exists("authorization", $info)) { //判断用户令牌是否存在
                $RequestAuthenticationAndUserAuthentication = RequestAuthenticationAndUserAuthentication(true, true, true, true, config('debug.debug'));
                if ($RequestAuthenticationAndUserAuthentication['code'] != 200) { //判断是否都验证通过了
                    #没有验证通过
                    return json($RequestAuthenticationAndUserAuthentication);
                } else {
                    //只有一个用户返回id
                    $user_info = $RequestAuthenticationAndUserAuthentication['data']['userVersion']['data']; //用户id
                    try { //用户喜欢表是从用户表登陆处查询出来的 不一定存在此表 
                        $commentLikeState = Db::table($user_info['userLikeTable']) //评论点赞状态 可直接判断
                            ->where([
                                "book_id" => $book_id,
                                "user_id" => $user_info["_id"],
                            ])
                            ->find();

                        if ($commentLikeState) {
                            //喜欢的话 设置为不喜欢
                            $deleteState = Db::table($user_info['userLikeTable']) //删除状态
                                ->where([
                                    "book_id" => $book_id,
                                    "user_id" => $user_info["_id"],
                                ])
                                ->delete();
                            Db::table('book_info')
                                ->where([
                                    "_id" => $book_id,
                                ])
                                ->dec("likes_count")
                                ->update();
                            if ($deleteState) {
                                $return = ["code" => "200", "message" => "success", "data" => ["action" => "unlike"]];
                            } else {
                                $return = ["code" => "02", "message" => "error:Cancel like failure/取消喜欢失败"];
                            }
                        } else {
                            //没有数据的时候 设置为喜欢
                            $insertState = Db::table($user_info['userLikeTable'])
                                ->insert([
                                    "book_id" => $book_id,
                                    "user_id" => $user_info["_id"],
                                    "like_id" => randIdStructure(1),
                                ]);
                            Db::table('book_info')
                                ->where([
                                    "_id" => $book_id,
                                ])
                                ->inc("likes_count")
                                ->update();
                            if ($insertState) {
                                $return = ["code" => "200", "message" => "success", "data" => ["action" => "like"]];
                            } else {
                                $return = ["code" => "03", "message" => "error:Setting like failed/设置喜欢失败"];
                            }
                        }
                    } catch (\Throwable $th) {
                        //@a 不存在表
                        return errorJsonReturn("Server-DataBase-10001", "请联系管理员", 0, [
                            "Line" => $th->getLine(),
                            "Code" => $th->getCode(),
                            "Message" => $th->getMessage(),
                            "File" => $th->getFile(),
                        ], false);
                    }
                }
            } else {
                $return =  ["code" => "01", "msg" => "缺少验证信息 请重试"];
            }
        }
        return json($return);
    }
}
