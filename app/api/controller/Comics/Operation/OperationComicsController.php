<?php
/*
 * @Date: 2020-09-12 23:36:27
 * @名称: 漫画 - 操作控制器
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑: 初雪桜
 * @LastEditTime: 2020-09-13 20:09:35
 * @FilePath: /Site/app/api/controller/Comics/Operation/OperationComicsController.php
 */

namespace app\api\controller\Comics\Operation;

use app\api\controller\Comics\Operation\Method\MethodLikeComics;
use app\BaseController;
use think\facade\Db;

class OperationComicsController extends BaseController
{
    public function LikeComics($book_id)
    {
        $MethodLikeComics = new MethodLikeComics($this->app);
        return $MethodLikeComics->StartLikeComics($book_id);
    }
}
