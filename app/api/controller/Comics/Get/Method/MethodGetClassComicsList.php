<?php
/*
 * @Date: 2020-09-12 23:36:27
 * @名称: 获取漫画分类下面的列表
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑: 初雪桜
 * @LastEditTime: 2020-09-12 23:55:41
 * @FilePath: /Site/app/api/controller/Comics/Get/Method/MethodGetClassComicsList.php
 */

namespace app\api\controller\Comics\Get\Method;

use app\BaseController;
use think\facade\Db;

class MethodGetClassComicsList extends BaseController
{
    /**
     * 获取某一分类的漫画列表
     *
     * @param String $name //@a 分类名
     * @param Int $page //@a 第几页
     * @return void
     */
    public function StartGetComicsList(
        $name, //@a 分类名
        $page //@a 第几页
    ) {
        //请求参数验证
        $RequestAuthenticationAndUserAuthentication = RequestAuthenticationAndUserAuthentication(true, true, true, true, config('debug.debug'));
        if ($RequestAuthenticationAndUserAuthentication['code'] != 200) { //判断是否都验证通过了
            #没有验证通过
            return json($RequestAuthenticationAndUserAuthentication);
        }
        //请求参数验证

        $classified_catalogue =  Db::table('classified_catalogue') //查询分类编号
            ->where([
                "title" => $name,
            ])
            ->field([
                "_id"
            ])
            ->find();
        if (!$classified_catalogue) { //查看我要查询的分类有没有
            return errorJsonReturn("发生意外错误 请联系管理员");
        }
        $dataBaseName = "book_info_categories_info_$classified_catalogue[_id]";
        $bookList = Db::table($dataBaseName)
            ->alias($dataBaseName)
            ->rightJoin("book_list book_list", "book_list._id = $dataBaseName.book_id") //查询漫画信息
            ->rightJoin("book_info book_info", "book_info._id = $dataBaseName.book_id") //查询漫画信息
            ->where([
                "book_list.state" => 1,
                "book_list.delete" => 0
            ])
            ->limit(20)
            ->order("book_list.id desc")
            ->field([
                "book_list.path", //封面路径
                "book_list.original_name", //封面保存名
                "book_list.file_server", //封面服务器地址
                "book_info._id as id", //漫画id
                "book_info.likes_count", //喜欢总数
                "book_info.pages_count", //总页数
                "book_info.total_views", //总查看人数
                "book_list.title", //标题
                "book_info.eps_count", //总分卷数量
                "book_info.tags", //标签
                "book_info.categories", //分类
                // "book_list.delete", //是否已删除
            ])
            ->order("$dataBaseName.id desc")
            ->select();

        $all_page = Db::table($dataBaseName)
            ->alias($dataBaseName)
            ->where(["state" => 1])
            ->count();

        $data["message"] = "success";
        $data["data"]['comics']['page'] = $page;
        $data['data']['comics']['pages'] = ceil($all_page / config("apiconfig.comicsAllClassSelectCount"));

        foreach ($bookList as $key => $value) {
            $author = Db::table('author_and_chinese_team') //查询作者
                ->alias('author_and_chinese_team')
                ->leftJoin("user_info  user_info", "user_info._id = author_and_chinese_team.user_id") //查询漫画信息
                ->where([
                    "author_and_chinese_team.state" => 0,
                    "author_and_chinese_team.book_id" => $value['id']
                ])
                ->field([
                    'author_and_chinese_team.user_id',
                    "author_and_chinese_team.author_or_chinese_team",
                    "user_info.name"
                ])
                ->select();
            $data["data"]['comics']['docs'][$key]['totalLikes'] = $value['likes_count'];
            // $data["data"]['comics']['docs'][$key]['categories'] = explode(',', $value['tags']);
            $data["data"]['comics']['docs'][$key]['categories'] = explode(',', $value['categories']);
            $data["data"]['comics']['docs'][$key]['thumb']['path'] =  $value['path'];
            $data["data"]['comics']['docs'][$key]['thumb']['originalName'] = $value['original_name'];
            $data["data"]['comics']['docs'][$key]['thumb']['fileServer'] =  $value['file_server'];
            $data["data"]['comics']['docs'][$key]['id'] = $value['id'];
            $data["data"]['comics']['docs'][$key]['likesCount'] =  $value['likes_count'];
            foreach ($author as $authorKey => $authorValue) {
                if ($authorValue['author_or_chinese_team'] == 1) {
                    $data["data"]['comics']['docs'][$key]['chineseTeam'][] = [
                        "name" => $authorValue['name'],
                        "user_id" => $authorValue['user_id']
                    ];
                } else {
                    $data["data"]['comics']['docs'][$key]['author'][] = [
                        "name" => $authorValue['name'],
                        "user_id" => $authorValue['user_id']
                    ];
                }
            }
            // $data["data"]['comics']['docs'][$key]['author'] = [0 => ["name" => "初雪桜", "user_id" => "5af393a7db91d02c83d987b0"], 1 => ["name" => "初雪桜", "user_id" => "5af393a7db91d02c83d987b0"]];
            // if ($key == 5) {
            //     $data["data"]['comics']['docs'][$key]['chineseTeam'] = [0 => ["name" => "初雪桜", "user_id" => "5af393a7db91d02c83d987b0"], 1 => ["name" => "初雪桜", "user_id" => "5af393a7db91d02c83d987b0"]];
            // }
            $data["data"]['comics']['docs'][$key]['pagesCount'] = $value['pages_count'] * 1;
            $data["data"]['comics']['docs'][$key]['_id'] =  $value['id'];
            $data["data"]['comics']['docs'][$key]['totalViews'] = $value['total_views'] * 1;
            $data["data"]['comics']['docs'][$key]['title'] = $value['title'];
            $data["data"]['comics']['docs'][$key]['epsCount'] = $value['eps_count'] * 1;
            $data["data"]['comics']['docs'][$key]['finished'] = false;
        }

        $data["data"]['comics']["total"]  = $all_page;
        $data["data"]['comics']["limit"]  = config("apiconfig.comicsAllClassSelectCount");
        $data['code'] = "200";
        return json($data);
    }
}
