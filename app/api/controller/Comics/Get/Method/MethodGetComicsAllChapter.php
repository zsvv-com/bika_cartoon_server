<?php
/*
 * @Date: 2020-09-12 23:36:27
 * @名称: 漫画 - 获取漫画分卷
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑: 初雪桜
 * @LastEditTime: 2020-09-13 00:11:11
 * @FilePath: /Site/app/api/controller/Comics/Get/Method/MethodGetComicsAllChapter.php
 */

namespace app\api\controller\Comics\Get\Method;

use app\BaseController;
use think\facade\Db;

class MethodGetComicsAllChapter extends BaseController
{

    public function StartGetComicsAllChapter(
        $page, //@a 第几页
        $book_id //@a 书籍id
    ) {
        //请求参数验证
        $RequestAuthenticationAndUserAuthentication = RequestAuthenticationAndUserAuthentication(true, true, true, true, config('debug.debug'));
        if ($RequestAuthenticationAndUserAuthentication['code'] != 200) { //判断是否都验证通过了
            #没有验证通过
            return json($RequestAuthenticationAndUserAuthentication);
        }
        //请求参数验证
        $data_json = Db::table("book_info_project")
            ->where("_id", "=", $book_id)
            ->limit($page * config("apiconfig.comicsAllChapterSelectCount") - config("apiconfig.comicsAllChapterSelectCount"), config("apiconfig.comicsAllChapterSelectCount"))
            ->field(
                [
                    "_id", //书籍id
                    "project_title as title", //标题
                    "order", //级数
                    "updated_at", //更新时间
                    "chapter_id as id", //分卷id
                ]
            )
            ->select();


        $data['data']['eps']['page'] =  $page * 1;


        $data['data']["eps"]['docs'] = [];
        foreach ($data_json as $key => $value) {
            array_push($data['data']["eps"]['docs'], $value);
        }
        $data['code'] = "200";
        $data['message'] = "success";
        //总页数
        $all_page = Db::table("book_info_project")
            ->where("_id", "=", $book_id)
            ->count();
        $data['data']['eps']['total'] =  count($data_json);
        $data['data']['eps']['limit'] =  config("apiconfig.comicsAllChapterSelectCount");
        $data['data']['eps']['pages'] = ceil($all_page / config("apiconfig.comicsAllChapterSelectCount"));
        return json($data);
    }
}
