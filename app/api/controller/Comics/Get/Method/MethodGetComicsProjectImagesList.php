<?php
/*
 * @Date: 2020-09-12 23:36:27
 * @名称: 漫画 - 获取分卷所有图片
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑: 初雪桜
 * @LastEditTime: 2020-09-13 21:18:17
 * @FilePath: /Site/app/api/controller/Comics/Get/Method/MethodGetComicsProjectImagesList.php
 */

namespace app\api\controller\Comics\Get\Method;

use app\BaseController;
use think\facade\Db;

class MethodGetComicsProjectImagesList extends BaseController
{
    /**
     * 获取分卷章节图片
     *
     * @param String $page //@a 第几页
     * @param String $bookId //@a 书籍id
     * @param String $chapterId //@a 分卷id
     * @return void
     */
    public function GetComicsProjectImagesList(
        $page,
        $bookId,
        $chapterId
    ) {
        $RequestAuthenticationAndUserAuthentication = RequestAuthenticationAndUserAuthentication(true, true, true, true, config('debug.debug'));
        if ($RequestAuthenticationAndUserAuthentication['code'] != 200) { //判断是否都验证通过了
            #没有验证通过
            return json($RequestAuthenticationAndUserAuthentication);
        }
        $selectCondition['_id'] = $bookId; //查询条件
        $selectCondition['project_id'] = $chapterId; //查询条件
        $data["code"] = "200";
        $data["message"] = "success";
        $data['data']["pages"]['docs'] = [];
        $project_info = Db::table('book_info_project') //查询分卷信息 获取分卷所在的表
            ->where([
                "_id" => $bookId,
                "chapter_id" => $chapterId,
            ])
            ->field([
                "database_table_name", //图片所在表 都是以 book_info_project_info_** 开头 如 book_info_project_info_1 book_info_project_info_2
            ])
            ->find();
        if (!$project_info) {
            return  json([
                "code" => 500, "error" => "Cannot read property '_id' of null", "message" => "--", "detail" => ":("
            ]);
        }
        $temp =  Db::table($project_info['database_table_name'])
            ->where($selectCondition)
            ->limit($page * config("apiconfig.comicsComicsInfoSelectCount") - config("apiconfig.comicsComicsInfoSelectCount"), config("apiconfig.comicsComicsInfoSelectCount"))
            ->field([
                "_id", //@s图片id
                "original_name", //@o 保存的文件名
                "image_server_host", //@q 图片 服务器地址
                "extension", //@a 图片扩展名
                "frame_id", //@d 图片路径
                //@x 请求应返回 image_server_host + static + 数据库地址 +frame_id 才可以正常访问图片 
                //@x 如 http://sakura.nat300.top/images/static/book_info_project_info_26/xs1zmx1odmao3u24iisurhctz
            ])
            ->select();
        if (count($temp) == 0) {
            //判断有没有数据 没有数据执行
            return  json([
                "code" => 500, "error" => "Cannot read property '_id' of null", "message" => "--", "detail" => ":("
            ]);
        }

        foreach ($temp as $key => $value) {
            $value["media"]["fileServer"] = $value['image_server_host'];
            $value["media"]["path"] = "$project_info[database_table_name]/$value[frame_id]";
            $value["media"]["originalName"] = "$value[original_name].$value[extension]";
            unset($value["image_server_host"]);
            unset($value["database_table_name"]);
            unset($value["frame_id"]);
            unset($value["original_name"]);
            $temp[$key] = $value;
        }
        $allPage = Db::table($project_info['database_table_name'])
            ->where($selectCondition)
            ->count();; //总页数
        $project_title =  Db::table("book_info_project")
            ->where([
                "_id" => $bookId,
                "chapter_id" => $chapterId,
            ])
            ->field([
                "project_title"
            ])
            ->find(); //分卷名
        $data['data']["pages"]['docs'] = $temp;
        $data["data"]['pages']["page"] = $page  * 1;
        $data["data"]['pages']["total"] = $allPage;
        $data["data"]['pages']["limit"] = config("apiconfig.comicsComicsInfoSelectCount");
        $data["data"]['pages']["pages"] = ceil($allPage / config("apiconfig.comicsComicsInfoSelectCount"));

        $data["data"]['ep']["_id"] = $bookId;
        $data["data"]['ep']["title"] = ($project_title) ? $project_title["project_title"] : "";
        //执行操作 添加阅读列表
        $mysqlTableName = "user_view_history_" . explode("-", $RequestAuthenticationAndUserAuthentication['data']['userVersion']['data']['_id'])[1][0]; //数据库表明
        if ($temp) {
            Db::table($mysqlTableName)
                ->insert([
                    "book_id" => $bookId,
                    "user_id" => $RequestAuthenticationAndUserAuthentication['data']['userVersion']['data']['_id'],
                    "add_time" => date("Y-m-d H:I:s"),
                    "_id" => randIdStructure(2, 255),
                    "state" => 0,
                ]);
        }
        return json($data);
    }
}
