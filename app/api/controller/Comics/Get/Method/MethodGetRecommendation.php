<?php
/*
 * @Date: 2020-09-12 23:36:27
 * @名称: 漫画 - 相关推荐
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑: 初雪桜
 * @LastEditTime: 2020-09-13 19:46:21
 * @FilePath: /Site/app/api/controller/Comics/Get/Method/MethodGetRecommendation.php
 */

namespace app\api\controller\Comics\Get\Method;

use app\BaseController;
use think\facade\Db;

class MethodGetRecommendation extends BaseController
{
    public function StartGetRecommendation(
        $book_id, //@a 书籍id
        $limit //@a 获取数据长度
    ) {
        //请求参数验证
        $RequestAuthenticationAndUserAuthentication = RequestAuthenticationAndUserAuthentication(true, true, true, true, config('debug.debug'));
        if ($RequestAuthenticationAndUserAuthentication['code'] != 200) { //判断是否都验证通过了
            #没有验证通过
            return json($RequestAuthenticationAndUserAuthentication);
        }
        //请求参数验证
        // $limit = input('get.limit', 10);
        # 看过的人也在看
        $data["code"] = "200";
        $data["message"] = "success";
        $data["data"]['comics'] = [];
        $bookinfo =  Db::table('book_info')
            ->where([
                "_id" => $book_id,
            ])
            ->find();

        $categories = explode(',', $bookinfo["categories"]);
        $keyWord = '';
        foreach ($categories as $key => $value) {
            $keyWord .= ".*$value+";
        }
        $bookList = Db::query("SELECT 
       `book_info`.`_id`,
       `book_info`.`finished`,
       `book_info`.`categories`,
       `book_info`.`pages_count`,
       `book_info`.`eps_count`,
       `book_info`.`likes_count`,
       `book_list`.`title`,
       `book_list`.`file_server`,
       `book_list`.`path`,
       `book_list`.`original_name`
     FROM `book_info` `book_info`
       INNER JOIN `book_list` ON `book_list`.`_id` = `book_info`.`_id`
        WHERE book_info.categories REGEXP '$keyWord'
         And book_info.id >= (
         SELECT floor(
             RAND() * (
               SELECT MAX(id)
               FROM `book_info`
               WHERE categories REGEXP '$keyWord'
             )
           )
       )
     LIMIT $limit;
     "); //随机查询十条数据  大约耗时 0.2秒
        foreach ($bookList as $key => $value) {
            $data["data"]['comics'][$key]["_id"] = $value['_id'];
            $data["data"]['comics'][$key]["id"] = $value['_id'];
            $data["data"]['comics'][$key]["title"] = $value['title'];
            $data["data"]['comics'][$key]["finished"] = ($value['finished'] == 0) ? true : false;
            $data["data"]['comics'][$key]["pagesCount"] = $value['pages_count']; //所有图片数量 //!减少数据库查询 已屏蔽 
            $data["data"]['comics'][$key]["epsCount"] = $value['eps_count']; //分卷数
            $data["data"]['comics'][$key]["categories"] = explode(',', $value["categories"]);
            $data["data"]['comics'][$key]['thumb']["originalName"] = $value['original_name'];
            $data["data"]['comics'][$key]['thumb']["path"] = $value['path'];
            $data["data"]['comics'][$key]['thumb']["fileServer"] = $value['file_server'];
            $data["data"]['comics'][$key]["likesCount"] = $value['likes_count'];
        }

        foreach ($data["data"]['comics'] as $key => $value) {
            // $data["data"]['comics'][$key]["pagesCount"] = Db::table('book_info_project_info')->where(['_id' => $value['_id']])->count(); //所有图片数量 //!减少数据库查询 已屏蔽 
            // $data["data"]['comics'][$key]["epsCount"] = Db::table('book_info_project')->where(['_id' => $value['_id']])->count(); //分卷数 //!减少数据库查询 已屏蔽 
            $author = Db::table('author_and_chinese_team') //查询作者
                ->alias('author_and_chinese_team')
                ->leftJoin("user_info  user_info", "user_info._id = author_and_chinese_team.user_id") //查询漫画信息
                ->where([
                    "author_and_chinese_team.state" => 0,
                    "author_and_chinese_team.book_id" => $value['_id']
                ])
                ->field([
                    'author_and_chinese_team.user_id',
                    "author_and_chinese_team.author_or_chinese_team",
                    "user_info.name"
                ])
                ->select();
            foreach ($author as $authorKey => $authorValue) {
                if ($authorValue['author_or_chinese_team'] == 1) {
                    $data["data"]['comics'][$key]['chineseTeam'][] = [
                        "name" => $authorValue['name'],
                        "user_id" => $authorValue['user_id']
                    ];
                } else {
                    $data["data"]['comics'][$key]['author'][] = [
                        "name" => $authorValue['name'],
                        "user_id" => $authorValue['user_id']
                    ];
                }
            }
        }

        return json($data);
    }
}
