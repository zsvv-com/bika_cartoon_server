<?php
/*
* @Date: 2020-09-12 23:34:38
* @名称: 漫画 - 获取 - 控制器
* @版本: 0.01
* @作者: 初雪桜
* @邮箱: 202184199@qq.com
* @最后编辑: 初雪桜
 * @LastEditTime: 2020-09-13 21:18:36
 * @FilePath: /Site/app/api/controller/Comics/Get/GetComicsController.php
*/

namespace app\api\controller\Comics\Get;

use app\api\controller\Comics\Get\Method\MethodGetClassComicsList;
use app\api\controller\Comics\Get\Method\MethodGetComicsAllChapter;
use app\api\controller\Comics\Get\Method\MethodGetComicsInfo;
use app\api\controller\Comics\Get\Method\MethodGetComicsProjectImagesList;
use app\api\controller\Comics\Get\Method\MethodGetRecommendation;
use app\BaseController;

class GetComicsController extends BaseController
{
    /**
     * 获取某一分类的漫画列表
     *
     * @param String $name //@a 分类名
     * @param Int $page //@a 第几页
     * @return void
     */
    public function GetClassComicsList(
        $comment_id, //@a 评论id 
        $comment_table_name //@a 评论所在的表的名称
    ) {
        $MethodGetComicsComment = new MethodGetClassComicsList($this->app);
        return $MethodGetComicsComment->StartGetComicsList(
            $comment_id, //@a 评论id 
            $comment_table_name //@a 评论所在的表的名称
        );
    }
    /**
     * 获取漫画详细信息
     *
     * @param String $book_id //@a 书籍id
     * @return void
     */
    public function GetComicsInfo(
        $book_id //@a 书籍id 
    ) {
        $MethodGetComicsComment = new MethodGetComicsInfo($this->app);
        return $MethodGetComicsComment->StartGetComicsInfo(
            $book_id //@a 书籍id 
        );
    }
    /**
     * 获取漫画分卷
     *
     * @param String $book_id //@a 书籍id
     * @return void
     */
    public function GetComicsAllChapter(
        $page,
        $book_id //@a 书籍id 
    ) {
        $MethodGetComicsComment = new MethodGetComicsAllChapter($this->app);
        return $MethodGetComicsComment->StartGetComicsAllChapter(
            $page,
            $book_id //@a 书籍id 
        );
    }
    /**
     * 获取漫画相关呢推荐
     *
     * @param String $book_id //@a 书籍id
     * @param Int $book_id //@a 获取几条数据
     * @return void
     */
    public function GetRecommendation(
        $book_id, //@a 书籍id 
        $limit //@a 获取数据长度
    ) {
        $MethodGetComicsComment = new MethodGetRecommendation($this->app);
        return $MethodGetComicsComment->StartGetRecommendation(
            $book_id, //@a 书籍id 
            $limit //@a 获取几个漫画 
        );
    }
    /**
     * 获取分卷章节图片
     *
     * @param String $page //@a 第几页
     * @param String $bookId //@a 书籍id
     * @param String $chapterId //@a 分卷id
     * @return void
     */
    public function GetComicsProjectImagesList(
        $page, //@a 第几页
        $bookId, //@a 书籍id
        $chapterId //@a 分卷id
    ) {
        $MethodGetComicsComment = new MethodGetComicsProjectImagesList($this->app);
        return $MethodGetComicsComment->GetComicsProjectImagesList(
            $page, //@a 第几页
            $bookId, //@a 书籍id
            $chapterId //@a 分卷id
        );
    }
}
