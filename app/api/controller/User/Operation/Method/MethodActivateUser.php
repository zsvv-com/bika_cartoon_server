<?php
/*
 * @Date: 2020-09-13 22:02:46
 * @名称: 用户 - 操作控制器 - 激活用户
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑: 初雪桜
 * @LastEditTime: 2020-09-13 23:03:37
 * @FilePath: /Site/app/api/controller/User/Operation/Method/MethodActivateUser.php
 */

namespace app\api\controller\User\Operation\Method;

use app\BaseController;
use think\facade\Db;

class MethodActivateUser extends BaseController
{
    /**
     * 用户激活
     *
     * @param String $AuthcodeStr //@a 用户授权码
     * @param String $AuthorizationVerification //@a 授权码验证
     * @return Html
     */
    public function ActivateUser($AuthcodeStr, $AuthorizationVerification)
    {
        $Sha256 = explode('/', $AuthorizationVerification)[0]; //AuthcodeStr 取 Authcode()的 encode 密钥为 $key3 
        $info = json_decode(urldecode(urlencode(authcode($AuthcodeStr, "DECODE", config('globalSettings.api_RegUserAuthcode')))), true); //解密出来加密的数据 先url解码 在json解码
        /* {#76 ▼
          +"time": 1592670951//此加密生成时间
          +"randStr": "u-pb7fqq-q5ddicvwg8djyjgc9f23toxnk4b4ndrgzy8q86lp7d3va75faeptbmly5pmawmarfc73yn7fs0cqaz1w8b0yag0ceqczi3qb6gj0vj7ip33u7op92xojl4haycxbckhp39o5tmcxvxzk1bdgmahtnml ▶"//随机字符串
          +"userName": "a12@q11q.com"//用户名 一般为邮箱
          +"userId": "u-pb7fqq-05hcj0imuxqmldqp2eykqts1ghoj3mcx108sec04rloi9jntl095plyamp1j9ozgru7v8bsnkd53418oplbwjallf2bdt5p9a6k2t0fxg4cj83gcc51vu64a24hj61g5v6akr6i3t74uqlgtb5c68je ▶"//用户id
          +"name": "我是谁"//昵称
        } */
        if (!$info) {
            // echo '解析参数失败 错误步骤1';
            return view('ErrorPage/error', [
                "errorCode" => "U-501",
                "errorTitle" => '激活账户出错',
                "errorMsg" => '解析参数失败->' . __LINE__,
            ]);
        }
        $HashHmac = urlencode(hash_hmac('sha256',  json_encode($info),  $info['randStr']));
        if ($HashHmac != $Sha256) {
            // echo '解析参数失败 错误步骤2';
            return view('ErrorPage/error', [
                "errorCode" => "U-501",
                "errorTitle" => '激活账户出错',
                "errorMsg" => '解析参数失败->' . __LINE__,
            ]);
        }
        if ($info['time'] < time()) { //判断验证码有效期是不是笔现在大
            if (sendActivation($info['userName'], $info['userId'], $info['name'])) {
                // return '激活时间超时 新的激活链接已发送邮箱';
                return view('ErrorPage/error', [
                    "errorCode" => "U-503",
                    "errorTitle" => '激活账户出错',
                    "errorMsg" => '新的激活链接已发送邮箱->' . __LINE__,
                ]);
            } else {
                // return '发件系统异常';
                return view('ErrorPage/error', [
                    "errorCode" => "U-502",
                    "errorTitle" => '激活账户出错',
                    "errorMsg" => '发件系统异常->' . __LINE__,
                ]);
            }
        }
        $user_info =  Db::table('user_info')
            ->where([
                "user_name" => $info['userName'],
                "_id" => $info['userId'],
                "name" => $info['name'],
            ])
            ->find();
        if (!$user_info || !$user_info['exp_state'] == 2 || !$user_info['state'] == 0) { //判断用户必须存在并且 未被激活 并且用户状态异常
            // return '激活异常 用户不存在 或者已被激活';
            return view('ErrorPage/error', [
                "errorCode" => "U-504",
                "errorTitle" => '激活账户出错',
                "errorMsg" => '激活异常 用户不存在 或者已被激活->' . __LINE__,
            ]);
        }
        Db::table('user_info')
            ->where([
                "user_name" => $info['userName'],
                "_id" => $info['userId'],
                "name" => $info['name'],
            ])
            ->update([
                "exp_state" => null, //'异常原因 1封禁 2待认证'
                "state" => 1, //'用户状态 1 正常 0异常'
                "certification_time" => date('Y-m-d H:i:s'),
            ]);
        // return '您已成功激活账户';
        return view('Success/Success', [
            "errorCode" => "Success",
            "errorTitle" => '您已成功激活账户',
            "url" => '/',
            "urlTitle" => '返回主页',

        ]);
    }
}
