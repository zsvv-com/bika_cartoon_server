<?php
/*
 * @Date: 2020-09-13 22:02:46
 * @名称: 用户 - 操作控制器 - 登陆控制器
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑: 初雪桜
 * @LastEditTime: 2020-09-13 22:20:17
 * @FilePath: /Site/app/api/controller/User/Operation/MetHod/MetHodUserLogin.php
 */

namespace app\api\controller\User\Operation\Method;

use app\BaseController;
use think\facade\Db;

class MetHodUserLogin extends BaseController
{
    /**
     * 用户登录
     *
     * @param String $userName //@a 用户名
     * @param String $passWord //@a 密码
     * @return Map
     */
    public function UserLogin($userName, $passWord)
    {
        $RequestAuthenticationAndUserAuthentication = RequestAuthenticationAndUserAuthentication(true, true, false, false, config('debug.debug'));
        if ($RequestAuthenticationAndUserAuthentication['code'] != 200) { //判断是否都验证通过了
            #没有验证通过
            return json($RequestAuthenticationAndUserAuthentication);
        }
        $requestState = $RequestAuthenticationAndUserAuthentication["data"]['requestVersion'];
        $userState = Db::table('user_info')
            ->alias("user_info")
            ->join('user_info_table_name', 'user_info_table_name._id = user_info._id')
            ->field([
                "user_info._id",
                "user_info.role",
                "user_info.name",
                "user_info_table_name.book_like_table_name",
                "user_info_table_name.comment_like_table_name",
            ])
            ->where([
                "user_info.user_name" => $userName,
                "user_info.pass_word" => $passWord,
            ])
            ->select();
        if (count($userState) != 1) {
            return errorJsonReturn("U-10002", false, 0, [], false);
        }
        // {"_id":"5af393a7db91d02c83d987b4","email":"202184199","role":"member","name":"鞠耀斌","version":"2.1.2.3","buildVersion":"32","platform":"ios","iat":1582973412,"exp":1583578212}
        $token = json_encode([
            "_id" => $userState[0]['_id'], //用户id
            "email" => $userName, //邮箱
            "role" => $userState[0]['role'], //身份
            "name" => $userState[0]['name'], //昵称
            "version" => $requestState['data']['version'], //软件版本
            "buildVersion" => $requestState['data']['buildVersion'], //编译版本
            "platform" => $requestState['data']['platform'], //系统版本 ios/android/web
            "iat" => time(), //申请时间
            "exp" => time() + 6048000, //过期时间
            "userLikeTable" => $userState[0]['book_like_table_name'], //用户喜欢漫画的时候所插入的表名
            "commentLikeTable" => $userState[0]['comment_like_table_name'], //用户喜欢的评论的表的id
        ]);
        $EncryptionMethod = json_encode([ //加密方式
            "Encryption" => "SHA256", //加密方法 假的误导用 
            "verification" => "SHA512",
        ]);
        $str1 = base64_encode($EncryptionMethod); //头部验证信息
        $str2 = base64_encode(authcode($token, 'encode', config('apiconfig.TokenResolutionKey'))); //加密字符串
        $str3 = hash_hmac("sha512", $str2, config('apiconfig.TokenResolutionKeySha512')); //取 加密字符串的 sha512

        $data["message"] = "success";
        $data["data"]["token"] = "$str1.$str2.$str3";
        $data["code"] = "200";

        $userState = Db::table('user_info')
            ->where(
                [
                    "_id" => $userState[0]["_id"],
                    "role" => $userState[0]["role"],
                    "name" => $userState[0]["name"],
                ]
            )
            ->update([
                "to_ken" => "$str1.$str2.$str3"
            ]);
        return json($data);
    }
}
