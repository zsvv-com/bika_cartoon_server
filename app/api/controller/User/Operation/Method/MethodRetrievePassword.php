<?php
/*
 * @Date: 2020-09-13 22:02:46
 * @名称: 用户 - 操作控制器 - 找回密码
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑: 初雪桜
 * @LastEditTime: 2020-09-14 19:31:01
 * @FilePath: /Site/app/api/controller/User/Operation/Method/MethodRetrievePassword.php
 */

namespace app\api\controller\User\Operation\Method;

use app\BaseController;
use think\facade\Db;

class MethodRetrievePassword extends BaseController
{
    /**
     * 取回密码
     *
     * @param String $userName //@a 用户名
     * @param String $NewPassWord //@a 新密码
     * @return String+email
     */
    public function RetrievePassword($userName, $NewPassWord)
    {

        if ($userName == null) {
            return errorJsonReturn('User name or password cannot be empty');
        }
        if ($NewPassWord == null) {
            return errorJsonReturn('User name or password cannot be empty');
        }
        if (!preg_match('/^(?![^a-zA-Z]+$)(?!\D+$).{6,}$/', $NewPassWord)) {
            return errorJsonReturn('Password must include alphanumeric', "密码必须包含数字字母");
        }
        $selectDb = Db::table('user_info')
            ->where(["user_name" => $userName])
            ->find();
        if (!$selectDb) {
            return errorJsonReturn('undefined user', "没有此用户信息");
        }
        if (!filter_var($userName, FILTER_VALIDATE_EMAIL)) {
            return errorJsonReturn('Please enter email', "请输入邮箱");
        }

        if (sendRetrievePassword($selectDb, $NewPassWord)) { //发送找回密码
            echo '激活邮件已发送至您的邮箱';
        } else {
            echo '发件系统异常';
        }
        return;
    }
}
