<?php
/*
 * @Date: 2020-09-13 22:02:46
 * @名称: 用户 - 操作控制器
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑: 初雪桜
 * @LastEditTime: 2020-09-14 19:51:43
 * @FilePath: /Site/app/api/controller/User/Operation/OperationUserController.php
 */

namespace app\api\controller\User\Operation;

use app\api\controller\User\Operation\Method\MethodActivateNewPassword;
use app\api\controller\User\Operation\Method\MethodActivateUser;
use app\api\controller\User\Operation\Method\MethodRetrievePassword;
use app\api\controller\User\Operation\Method\MetHodUserLogin;
use app\api\controller\User\Operation\Method\MethodUserReg;
use app\BaseController;

class OperationUserController extends BaseController
{
    /**
     * 用户登录
     *
     * @param String $userName //@a 用户名
     * @param String $passWord //@a 密码
     * @return Map
     */
    public function LoginUser($userName, $passWord)
    {
        $MetHodUserLogin = new MetHodUserLogin($this->app);
        return $MetHodUserLogin->UserLogin($userName, $passWord);
    }
    /**
     * 用户注册
     *
     * @param String $userName //@a 用户名
     * @param String $name //@a 昵称
     * @param String $passWord //@a 密码
     * @param String $gender //@a 性别
     * @return Map
     */
    public function RegUser($userName, $name, $passWord, $gender)
    {
        $MethodUserReg = new MethodUserReg($this->app);
        return $MethodUserReg->UserReg($userName, $name, $passWord, $gender);
    }
    /**
     * 用户激活
     *
     * @param String $AuthcodeStr //@a 用户授权码
     * @param String $AuthorizationVerification //@a 授权码验证
     * @return Html
     */
    public function ActivateUser($AuthcodeStr, $AuthorizationVerification)
    {
        $MethodUserReg = new MethodActivateUser($this->app);
        return $MethodUserReg->ActivateUser($AuthcodeStr, $AuthorizationVerification);
    }
    /**
     * 修改密码
     *
     * @param String $userName //@a 用户名
     * @param String $NewPassWord //@a 密码
     * @return Html
     */
    public function RetrievePassword($userName, $NewPassWord)
    {
        $MethodRetrievePassword = new MethodRetrievePassword($this->app);
        return $MethodRetrievePassword->RetrievePassword($userName, $NewPassWord);
    }
    /**
     * 激活新密码
     *
     * @param String $AuthcodeStr //@a 用户信息
     * @param String $Sha256 //@a 验证
     * @return Html
     */
    public function ActivateNewPassword($AuthcodeStr, $Sha256)
    {
        $MethodActivateNewPassword = new MethodActivateNewPassword($this->app);
        return $MethodActivateNewPassword->ActivateNewPassword($AuthcodeStr, $Sha256);
    }
}
