<?php
/*
 * @Date: 2020-09-13 22:02:46
 * @名称: 用户 - 操作控制器
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑: 初雪桜
 * @LastEditTime: 2020-09-14 21:37:43
 * @FilePath: /Site/app/api/controller/User/Get/GetUserController.php
 */

namespace app\api\controller\User\Get;

use app\api\controller\User\Get\Method\MethodGetFavourite;
use app\api\controller\User\Get\Method\MethodGetUserInfo;
use app\BaseController;

class GetUserController extends BaseController
{
    /**
     * 获取用户信息
     * 
     * @return Map
     */
    public function GetUserInfo()
    {
        $MethodGetUserInfo = new MethodGetUserInfo($this->app);
        return $MethodGetUserInfo->GetUserInfo();
    }
    /**
     * 获取用户收藏
     * 
     * @return Map
     */
    public function GetFavourite()
    {
        $MethodGetFavourite = new MethodGetFavourite($this->app);
        return $MethodGetFavourite->GetFavourite();
    }
}
