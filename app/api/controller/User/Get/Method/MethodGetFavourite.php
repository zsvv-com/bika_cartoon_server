<?php
/*
 * @Date: 2020-09-13 22:02:46
 * @名称: 用户 - 获取控制器 - 获取用户收藏
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑: 初雪桜
 * @LastEditTime: 2020-09-14 21:31:27
 * @FilePath: /Site/app/api/controller/User/Get/Method/MethodGetFavourite.php
 */

namespace app\api\controller\User\Get\Method;

use app\api\model\UserInfo;
use app\BaseController;
use think\facade\Db;

class MethodGetFavourite extends BaseController
{
    /**
     * 激活新密码
     *
     * @param String $AuthcodeStr //@a 用户信息
     * @param String $Sha256 //@a 验证字符串
     * @return String+email
     */
    public function GetFavourite()
    {
        $page = input('get.page', 1, 'int');
        if ($page <= 0) { //判断page是不是数字
            $page = 1;
        }
        $RequestAuthenticationAndUserAuthentication = RequestAuthenticationAndUserAuthentication(true, true, true, true, config('debug.debug'));
        if ($RequestAuthenticationAndUserAuthentication['code'] != 200) { //判断是否都验证通过了
            #没有验证通过
            return json($RequestAuthenticationAndUserAuthentication);
        }

        $user_id = $RequestAuthenticationAndUserAuthentication['data']['userVersion']['data']['_id'];
        $mysqlTableName = "user_favourite_" . explode("-", $RequestAuthenticationAndUserAuthentication['data']['userVersion']['data']['_id'])[1][0]; //数据库表明
        $userlike = Db::table($mysqlTableName)
            ->where([
                "user_id" => $user_id,
            ])
            ->limit($page * config("apiconfig.UserFavouriteSelectNumber") - config("apiconfig.UserFavouriteSelectNumber"), config("apiconfig.UserFavouriteSelectNumber"))
            ->select();
        $data['code'] = "200";
        $data['message'] = "success";
        if (count($userlike)) {
            foreach ($userlike as $key => $value) {
                $bookInfo = Db::table('book_info')
                    ->where(["book_list._id" => $value['book_id']])
                    ->join("book_list book_list", "book_list._id = book_info._id")
                    ->field([
                        "book_info._id",
                        "book_info.pages_count",
                        "book_info.eps_count",
                        "book_info.categories",
                        "book_info.finished",
                        "book_info.likes_count",
                        "book_list.title",
                        "book_list.original_name",
                        "book_list.path",
                        "book_list.file_server",
                        "book_list.user_id",
                    ])
                    ->find();
                $data['data']['comics']['docs'][$key]['_id'] = $value['book_id'];
                $data['data']['comics']['docs'][$key]['title'] = $bookInfo['title'];
                $data['data']['comics']['docs'][$key]['author'] = UserInfo::where(["_id" => $bookInfo['user_id']])->find()->name;
                $data['data']['comics']['docs'][$key]['pagesCount'] =  $bookInfo['pages_count'];
                $data['data']['comics']['docs'][$key]['epsCount'] =  $bookInfo['eps_count'];
                $data['data']['comics']['docs'][$key]['finished'] =  ($bookInfo["finished"]) ? true : false;
                $data['data']['comics']['docs'][$key]['categories'] = explode(',', $bookInfo['categories']);
                $data['data']['comics']['docs'][$key]['thumb']['originalName'] = $bookInfo['original_name'];
                $data['data']['comics']['docs'][$key]['thumb']['path'] = $bookInfo['path'];
                $data['data']['comics']['docs'][$key]['thumb']['fileServer'] = $bookInfo['file_server'];
                $data['data']['comics']['docs'][$key]['id'] = $value['book_id'];
                $data['data']['comics']['docs'][$key]['likesCount'] = $bookInfo['likes_count'];
            }
        } else {
            $data['data']['comics']['docs'] = [];
        }

        $data['data']['comics']['total']  =  Db::table($mysqlTableName)
            ->where([
                "user_id" => $user_id,
            ])
            ->count();;
        $data['data']['comics']['limit']  = config("apiconfig.UserFavouriteSelectNumber");
        $data['data']['comics']['page']  = $page;
        $data['data']['comics']['pages']  = ceil($data['data']['comics']['total'] / config("apiconfig.UserFavouriteSelectNumber"));

        return json($data);
    }
}
