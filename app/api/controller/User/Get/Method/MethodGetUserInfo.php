<?php
/*
 * @Date: 2020-09-13 22:02:46
 * @名称: 用户 - 获取控制器 - 获取用户信息
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑: 初雪桜
 * @LastEditTime: 2020-09-14 21:26:50
 * @FilePath: /Site/app/api/controller/User/Get/Method/MethodGetUserInfo.php
 */

namespace app\api\controller\User\Get\Method;

use app\BaseController;
use think\facade\Db;

class MethodGetUserInfo extends BaseController
{
    /**
     * 激活新密码
     *
     * @param String $AuthcodeStr //@a 用户信息
     * @param String $Sha256 //@a 验证字符串
     * @return String+email
     */
    public function GetUserInfo()
    {
        $RequestAuthenticationAndUserAuthentication = RequestAuthenticationAndUserAuthentication(true, true, true, true, config('debug.debug'));
        if ($RequestAuthenticationAndUserAuthentication['code'] != 200) { //判断是否都验证通过了
            #没有验证通过
            return json($RequestAuthenticationAndUserAuthentication);
        }
        //当正确返回 会有以下数据 这是来自 token 的
        //* ^ array:3 [
        //*     "code" => 200
        //*     "message" => "success"
        //*     "data" => array:3 [
        //*       "appVersion" => array:3 [  app验证
        //*         "code" => 200
        //*         "message" => "success"
        //*         "data" => array:1 [
        //*           "private_key" => "DGNMO8NlJw3aqh6opP7gTbpUta23Zk0I4xJthrE0FpB3PKzhinyBsZjYHtXuxEi99lPhfUAGhVUrr5M5OxYtZF3V18G8ZM5AEZmkcaDn3x1rBZPXS5TAynGkhFdZu0pj26uWiSDXiocGoGheZ8DtTE3K2dbKdBKf6OtjfxbVD5hxDRhqkAA34sIBzYRsDPRRiCj2Xn5CNQXxvMSHiEyQMjxpskXuKOFimXr3oG4wjGZIqjcvWg3gM4xwYQ0PLAw"
        //*         ]
        //*       ]
        //*       "requestVersion" => array:3 [
        //*         "code" => 200
        //*         "message" => "success"
        //*         "data" => array:3 [
        //*           "version" => "2.1.2.6"
        //*           "buildVersion" => "33"
        //*           "platform" => "ios"
        //*         ]
        //*       ]
        //*       "userVersion" => array:3 [  用户验证
        //*         "code" => 200
        //*         "message" => "success"
        //*         "data" => array:9 [
        //*           "_id" => "user-sg8h6var5fp43m0zqb2wrs4jjihby9yo548k0j1yiu42n8qhwb1h5b1sqkj4pj3exi41y9wsd145sbsm6247efxh9gjz9b7ebrcmr2xnf0bbv7b5y2s7974qaw71f1ga7fq74ceeblvfmetb5ubwflrzonuhpn0xkbqi4wsds4p87f0n8fjcspi5wt8alvffzfizv69xeacjh4s8wcf2lwg9td5rxxe84yxu4fcikitd22mu4xrnh"
        //*           "email" => "sakura_223@icloud.com"
        //*           "role" => "member"
        //*           "name" => "鞠耀斌"
        //*           "version" => "2.1.2.6"
        //*           "buildVersion" => "33"
        //*           "platform" => "ios"
        //*           "iat" => 1593748953
        //*           "exp" => 1594353753
        //*         ]
        //*       ]
        //*     ]
        //*   ]
        $selectUserInfo = $RequestAuthenticationAndUserAuthentication['data']['userVersion']['data'];
        //!数据库查询用户信息
        $user_info = Db::table('user_info')
            ->where([
                "_id" => $selectUserInfo['_id'],
                "user_name" => $selectUserInfo['email'],
                "name" => $selectUserInfo['name'],
            ])
            ->field([
                "characters", //爱好
                "exp", //经验
                "application_time as created_at", //注册时间
                "verified", //是否认证
                "_id", //用户id
                "birthday", //生日
                "character", //头像框
                "user_name as email", //用户邮箱
                "gender", //性别 0机器人 1男性 2 女性
                "name", //昵称
                "slogan", //个性签名
            ])
            ->find();
        // dump($user_info);
        $data['message'] = "success";
        $data['data']['user']['characters'] = explode(',', $user_info['characters']); //爱好
        $data['data']['user']["created_at"] = $user_info['created_at']; //注册时间
        $data['data']['user']["verified"] = $user_info['verified'] ? true : false; //是否认证
        $data['data']['user']["_id"] = $user_info['_id']; //id

        $level_info = Db::table('user_level')
            ->where([
                ["min_experience", "<=", $user_info['exp']],
                ["max_experience", ">=", $user_info['exp']]
            ])
            ->field([
                "level_name", //等级名
                "level", //等级
                "max_experience", //最大经验值
            ])
            ->find();
        $data['data']['user']["exp"] =   $user_info['exp'] / $level_info['max_experience']; //经验 百分比
        $data['data']['user']["title"] = $level_info['level_name']; //等级名称
        $data['data']['user']["level"] = $level_info['level']; //等级
        $data['data']['user']["birthday"] = $user_info['birthday']; //生日
        $data['data']['user']["character"] = $user_info['character']; //头像框
        $data['data']['user']["email"] = $user_info['email']; //邮箱
        $data['data']['user']["slogan"] = $user_info['slogan']; //个性签名

        if ($user_info['gender'] == 2) {
            $data['data']['user']["gender"] = "m"; //性别
        } else if ($user_info['gender'] == 1) {
            $data['data']['user']["gender"] = "g"; //性别
        } else {
            $data['data']['user']["gender"] = "bot"; //性别
        }
        $data['data']['user']["name"] = $user_info['name']; //昵称
        $data['code'] = "200";
        return json($data);
    }
}
