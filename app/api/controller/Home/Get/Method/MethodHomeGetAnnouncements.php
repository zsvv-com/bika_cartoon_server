<?php
/*
 * @Date: 2020-09-13 21:19:53
 * @名称: 主页 - 获取控制器 - 获取公告
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑: 初雪桜
 * @LastEditTime: 2020-09-13 21:34:28
 * @FilePath: /Site/app/api/controller/Home/Get/Method/MethodHomeGetAnnouncements.php
 */

namespace app\api\controller\Home\Get\Method;

use app\BaseController;
use think\facade\Db;

class MethodHomeGetAnnouncements extends BaseController
{
    public function GetAnnouncements()
    {
        //请求参数验证
        $RequestAuthenticationAndUserAuthentication = RequestAuthenticationAndUserAuthentication(true, true, true, true, config('debug.debug'));
        if ($RequestAuthenticationAndUserAuthentication['code'] != 200) { //判断是否都验证通过了
            #没有验证通过
            return json($RequestAuthenticationAndUserAuthentication);
        }
        $data["code"] =   "200";
        $data["message"] = "success";
        $data['data']["announcements"] = array();
        $data['data']['announcements']["page"] = "1";
        $data['data']['announcements']["pages"] = 7;
        $data['data']["total"] = 31;
        $data['data']["limit"] = 5;
        $date = Db::table("announcements") //查询数据库 条件是 开启等于一的
            ->where("enable", 1)
            ->field("_id,title,content,created_at,original_name as originalName , path,file_server as fileServer")
            ->select();
        foreach ($date as $key => $value) {
            $value["thumb"]["fileServer"] = $value['fileServer'];
            $value["thumb"]["path"] = $value['path'];
            $value["thumb"]["originalName"] = $value['originalName'];
            unset($value["fileServer"]);
            unset($value["path"]);
            unset($value["originalName"]);
            $date[$key] = $value;
        }
        $data['data']["announcements"]['docs'] = $date;
        return json($data);
    }
}
