<?php
/*
 * @Date: 2020-09-13 21:19:53
 * @名称: 主页 - 获取控制器 - 获取横幅
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑: 初雪桜
 * @LastEditTime: 2020-09-13 21:29:33
 * @FilePath: /Site/app/api/controller/Home/Get/Method/MethodHomeGetBanner.php
 */

namespace app\api\controller\Home\Get\Method;

use app\BaseController;
use think\facade\Db;

class MethodHomeGetBanner extends BaseController
{
    public function GetBanner()
    {
        //请求参数验证
        $RequestAuthenticationAndUserAuthentication = RequestAuthenticationAndUserAuthentication(true, true, true, true, config('debug.debug'));
        if ($RequestAuthenticationAndUserAuthentication['code'] != 200) { //判断是否都验证通过了
            #没有验证通过
            return json($RequestAuthenticationAndUserAuthentication);
        }
        $data["code"] = "200";
        $data["message"] = "success";

        $date = Db::table("banner") //查询数据库 条件是 开启等于一的
            ->where("enable", 1)
            ->field("_id,title,short_description as shortDescription,type,link,file_server as fileServer,path , original_name as originalName")
            ->select();
        foreach ($date as $key => $value) {
            if ($value["type"] == 0) {
                $value["type"] = "web";
            }
            $value["thumb"]["fileServer"] = $value['fileServer'];
            $value["thumb"]["path"] = $value['path'];
            $value["thumb"]["originalName"] = $value['originalName'];
            unset($value["fileServer"]);
            unset($value["path"]);
            unset($value["originalName"]);
            $date[$key] = $value;
        }
        $data['data']["banners"] = $date;
        return json($data);
    }
}
