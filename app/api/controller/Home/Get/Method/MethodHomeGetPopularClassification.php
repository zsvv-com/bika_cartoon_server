<?php
/*
 * @Date: 2020-09-13 21:19:53
 * @名称: 主页 - 获取控制器 - 获取公告
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑: 初雪桜
 * @LastEditTime: 2020-09-13 21:35:55
 * @FilePath: /Site/app/api/controller/Home/Get/Method/MethodHomeGetPopularClassification.php
 */

namespace app\api\controller\Home\Get\Method;

use app\BaseController;
use think\facade\Db;

class MethodHomeGetPopularClassification extends BaseController
{
    public function GetPopularClassification()
    {
        //请求参数验证
        $RequestAuthenticationAndUserAuthentication = RequestAuthenticationAndUserAuthentication(true, true, true, true, config('debug.debug'));
        if ($RequestAuthenticationAndUserAuthentication['code'] != 200) { //判断是否都验证通过了
            #没有验证通过
            return json($RequestAuthenticationAndUserAuthentication);
        }
        //请求参数验证

        $data['code'] = "200";
        $data['message'] = "success";
        $date = Db::table("classified_catalogue")
            ->where([
                "enable" => 1,
                "state" => 1,
            ])
            ->field("_id,title,file_server as fileServer,path ,original_name as originalName , is_web as isWeb,active,link,description")
            ->select();
        foreach ($date as $key => $value) {
            $value["thumb"]["fileServer"] = $value['fileServer'];
            $value["thumb"]["path"] = $value['path'];
            $value["thumb"]["originalName"] = $value['originalName'];
            unset($value["fileServer"]);
            unset($value["path"]);
            unset($value["originalName"]);
            if ($value["_id"] == '未定义') {
                unset($value["_id"]);
            }
            if ($value["isWeb"]) {
                $value["isWeb"] = true;
            } else {
                $value["isWeb"] = false;
                unset($value["link"]);
            }
            if ($value["active"]) {
                $value["active"] = true;
            } else {
                $value["active"] = false;
            }


            if ($value["isWeb"] && $value["active"]) {
                unset($value["description"]);
            }
            if (!$value["isWeb"] && !$value["active"]) {
                unset($value["isWeb"]);
                unset($value["active"]);
            }
            $date[$key] = $value;
        }
        $data['data']["categories"] = $date;

        return json($data);
    }
}
