<?php
/*
 * @Date: 2020-09-13 21:19:53
 * @名称: 主页 - 获取控制器
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑: 初雪桜
 * @LastEditTime: 2020-09-13 21:36:56
 * @FilePath: /Site/app/api/controller/Home/Get/GetHomeController.php
 */

namespace app\api\controller\Home\Get;

use app\api\controller\Home\Get\Method\MethodHomeGetAnnouncements;
use app\api\controller\Home\Get\Method\MethodHomeGetBanner;
use app\api\controller\Home\Get\Method\MethodHomeGetPopularClassification;
use app\BaseController;
use think\facade\Db;

class GetHomeController extends BaseController
{
    public function HomeGetBanner()
    {
        $MethodHomeGetBanner = new MethodHomeGetBanner($this->app);
        return $MethodHomeGetBanner->GetBanner();
    }
    public function HomeGetAnnouncements()
    {
        $MethodHomeGetBanner = new MethodHomeGetAnnouncements($this->app);
        return $MethodHomeGetBanner->GetAnnouncements();
    }
    public function HomeGetPopularClassification()
    {
        $MethodHomeGetPopularClassification = new MethodHomeGetPopularClassification($this->app);
        return $MethodHomeGetPopularClassification->GetPopularClassification();
    }
}
