<?php
/*
 * @Date: 2020-01-31 13:41:09
 * @名称: 漫画列表
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-09-13 21:39:14
 * @FilePath: /Site/app/api/controller/Comics.php
 */


namespace app\api\controller;

use app\api\controller\Comics\Get\GetComicsController;
use app\api\controller\Comics\Get\Method\MethodGetComicsProjectImagesList;
use app\api\controller\Comics\Operation\OperationComicsController;
use app\BaseController;
use think\facade\Db;
use think\facade\Request;

class Comics extends BaseController
{
    //@x 获取分类下的漫画列表
    public function Index()
    {
        $name = input('classified', "嗶咔漢化");
        $page = input('get.page', 1);
        $GetComicsController = new GetComicsController($this->app);
        return $GetComicsController->GetClassComicsList($name, $page);
    }
    //@x 获取书籍详细信息
    public function ComicsInfo()
    {
        $book_id = input('get.book_id');
        $GetComicsController = new GetComicsController($this->app);
        return $GetComicsController->GetComicsInfo($book_id);
    }
    //@x 获取漫画分卷
    public function AllChapter()
    {
        $page = input('page');
        $book_id = input('book_id');
        $GetComicsController = new GetComicsController($this->app);
        return $GetComicsController->GetComicsAllChapter($page, $book_id);
    }
    //@x 相关推荐
    public function Recommendation() //相关推荐
    {
        $book_id = input('get.book_id');
        $limit = input('get.limit', 10);
        $GetComicsController = new GetComicsController($this->app);
        return $GetComicsController->GetRecommendation($book_id, $limit);
    }
    //@x 获取某个分卷的图片列表
    public function GetComicsProjectImagesList()
    {
        $page = input('page', 1);
        $bookId = input("get.bookId");
        $chapterId = input("get.chapterId");
        $OperationComicsController = new GetComicsController($this->app);
        return $OperationComicsController->GetComicsProjectImagesList($page, $bookId, $chapterId);
    }
    //@x 喜欢/不喜欢漫画
    public function like()
    {
        $_book_id = input('post.book_id');
        $OperationComicsController = new OperationComicsController($this->app);
        return $OperationComicsController->LikeComics($_book_id);
    }
}
