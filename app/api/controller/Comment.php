<?php
/*
 * @Date: 2020-01-31 13:41:09
 * @名称: 评论
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-09-16 21:09:44
 * @FilePath: /Site/app/api/controller/Comment.php
 */

namespace app\api\controller;

use app\api\controller\Comment\Get\GetCommentController;
use app\api\controller\Comment\Operation\OperationCommentController;
use app\api\controller\Comment\Publishing\PublishingController;
use app\BaseController;
use think\facade\Db;
use think\facade\Request;

class Comment extends BaseController
{
    public function Index()
    {
        $bookId = input("get.bookId");
        $subsectionId = input("get.subsectionId", null);
        $page = (int) input("get.page", 1);
        $GetCommentController = new GetCommentController($this->app);
        return $GetCommentController->GetComicsComment(
            $bookId, //@x 漫画id
            $subsectionId, //@x 分卷id
            $page //@x 第几页
        );
    }

    public function SonComment()
    {
        $page = input('get.page', 1);
        $comment_id = input('get.comment_id'); //@a 评论的id
        $book_id = input('post.book_id');
        $comment_table_name = input('post.comment_table_name'); //@v 新参数 主评论表的名字
        $GetCommentController = new GetCommentController($this->app);
        return $GetCommentController->GetCommentSonComment(
            $page, //@x 第几页
            $comment_id, //@x 获取那个评论额id
            $book_id, //@x 那一本书籍的
            $comment_table_name //@x 这个评论所在的表
        );
    }

    public function like()
    {

        //请求参数验证
        $comment_id = input('post.comment_id');
        $comment_table_name = input('post.comment_table_name');
        $OperationCommentController = new OperationCommentController($this->app);
        return $OperationCommentController->LikeComment(
            $comment_id, //@a 评论id 
            $comment_table_name //@a 评论所在的表的名称
        );
    }
    public function SendComment()
    {

        $_book_id = input('post.book_id');
        $_content = input('post.content'); //!评论的内容
        $_comment_table_name = input('post.comment_table_name', null); //@v 新参数 主评论表的名字
        $_project_id = input('post.project_id', null); //分卷id
        $_comment_id = input('post.comment_id', null); //回复的楼层的id
        $RequestAuthenticationAndUserAuthentication = RequestAuthenticationAndUserAuthentication(true, true, true, true, config('debug.debug'));
        if ($RequestAuthenticationAndUserAuthentication['code'] != 200) { //判断是否都验证通过了
            #没有验证通过
            return json($RequestAuthenticationAndUserAuthentication);
        }
        $user_info = $RequestAuthenticationAndUserAuthentication['data']['userVersion']['data'];
        if ($_book_id == null || $_content == null) { //判断有没有传入书籍id
            return errorJsonReturn("Comment-Main-10002");
        }

        if ($_comment_id == null) { // 回复的父id必须为空
            //@a 这是评论漫画的子章节
            //@a -------------------------------- 发布评论 --------------------------------
            $CommentController = new PublishingController($this->app);
            return $CommentController->Publishing($_book_id, $_content, $user_info, $_project_id);
            //@a -------------------------------- 发布评论 --------------------------------
        }
        if ($_comment_id) { // 回复的父id必须为空
            //@a 这是评论漫画的子章节
            //@a -------------------------------- 发布评论 --------------------------------
            $CommentController = new PublishingController($this->app);
            return $CommentController->Reply($_book_id, $_content, $user_info, $_comment_id, $_comment_table_name);
            //@a -------------------------------- 发布评论 --------------------------------
        }
    }
}
