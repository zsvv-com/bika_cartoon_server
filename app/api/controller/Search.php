<?php
/*
 * @Date: 2020-01-31 13:41:09
 * @名称: 漫画列表
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-26 17:50:55
 * @FilePath: /服务器/app/api/controller/Search.php
 */


namespace app\api\controller;

use app\BaseController;
use think\Config;
use think\facade\Db;
use think\facade\Request;

class Search extends BaseController
{
    public function Index()
    { //请求参数验证

        $RequestAuthenticationAndUserAuthentication = RequestAuthenticationAndUserAuthentication(true, true, true, true, config('debug.debug'));
        if ($RequestAuthenticationAndUserAuthentication['code'] != 200) { //判断是否都验证通过了
            #没有验证通过
            return json($RequestAuthenticationAndUserAuthentication);
        }
        //请求参数验证
        $keyword = input('post.keyword');
        $sort = input('post.sort');
        $categories = input('post.categories'); //搜索所在的分类入 全彩,妹妹系
        $categories = explode(",", $categories);
        switch ($sort) {
            case 'ua': //@j 默认
                $sort = "book_info.id desc";
                break;
            case 'da': //@j 旧到新
                $sort = "book_info.id asc";
                break;
            case 'ld': //@j 最多爱心
                $sort = "book_info.likes_count";
                break;
            case 'vd': //@j 最多指名
                $sort = "book_info.views_count";
                break;
            default:
                $sort = "book_info.id desc";
                break;
        }
        $where[] = ['book_list.title|book_info.description|book_info.tags|book_info.categories', 'like', "%" . $keyword . "%"];
        foreach ($categories as $key => $value) { //寻找所在的分类
            $categoriesWhere[] = ['book_info.categories', 'like', "%" . $value . "%"];
        }
        $page = input('get.page', 1);
        $bookList = Db::table("book_list")
            ->alias('book_list')
            ->join('book_info book_info', "book_info._id = book_list._id")
            ->where($where)
            ->where($categoriesWhere)
            ->field([
                "book_list.path", //封面路径
                "book_list.original_name", //封面保存名
                "book_list.file_server", //封面服务器地址
                "book_info._id as id", //漫画id
                "book_info.likes_count", //喜欢总数
                "book_info.pages_count", //总页数
                "book_info.total_views", //总查看人数
                "book_list.title", //标题
                "book_info.eps_count", //总分卷数量
                "book_info.tags", //标签
                "book_info.categories", //分类
            ])
            ->limit($page * config("apiconfig.searchSelectCount") - config("apiconfig.searchSelectCount"), config("apiconfig.searchSelectCount"))
            ->order($sort)
            ->select();
        $all_page = Db::table('book_list')
            ->alias('book_list')
            ->join('book_info book_info', "book_info._id = book_list._id")
            ->where($where)
            ->where($categoriesWhere)
            ->count();
        $data["message"] = "success";
        $data["data"]['comics']['page'] = $page;
        $data['data']['comics']['pages'] = ceil($all_page / config("apiconfig.comicsAllClassSelectCount"));
        $chineseTeam = [];
        $author = [];
        foreach ($bookList as $key => $value) {
            $author = Db::table('author_and_chinese_team') //查询作者
                ->alias('author_and_chinese_team')
                ->leftJoin("user_info  user_info", "user_info._id = author_and_chinese_team.user_id") //查询漫画信息
                ->where([
                    "author_and_chinese_team.state" => 0,
                    "author_and_chinese_team.book_id" => $value['id']
                ])
                ->field([
                    'author_and_chinese_team.user_id',
                    "author_and_chinese_team.author_or_chinese_team",
                    "user_info.name"
                ])
                ->select();
            foreach ($author as $authorKey => $authorValue) {
                if ($authorValue['author_or_chinese_team'] == 1) {
                    $chineseTeam[] = [
                        "name" => $authorValue['name'],
                        "user_id" => $authorValue['user_id']
                    ];
                } else {
                    $author[] = [
                        "name" => $authorValue['name'],
                        "user_id" => $authorValue['user_id']
                    ];
                }
            }

            $data["data"]['comics']['docs'][] = [
                "totalLikes" => $value['likes_count'],
                "categories" => explode(',', $value['categories']),
                "thumb" => [
                    "path" => $value['path'],
                    "originalName" => $value['original_name'],
                    "fileServer" => $value['file_server'],
                ],
                "id" => $value['id'],
                "likesCount" => $value['likes_count'],
                "chineseTeam" => $chineseTeam,
                "author" => $author,
                "pagesCount" => $value['pages_count'] * 1,
                "_id" =>  $value['id'],
                "totalViews" => $value['total_views'] * 1,
                "title" => $value['title'],
                "epsCount" => $value['eps_count'] * 1,
                "finished" => false,
            ];
        }

        $data["data"]['comics']["total"]  = $all_page;
        $data["data"]['comics']["limit"]  = config("apiconfig.comicsAllClassSelectCount");
        $data['code'] = "200";
        return json($data);
        // return response($data, 200)->contentType("application/json")->header(["Content-Type: application/json"]);
    }
}
