<?php
/*
 * @Date: 2020-04-27 17:14:15
 * @名称: 
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-09-14 21:40:18
 * @FilePath: /Site/app/api/route/route.php
 */

use think\facade\Route;
//注册路由
//@------------------------------------起始页------------------------------------
Route::rule('ServiceList', 'ServiceList/Index', 'get'); //?漫画 - 主页 - 首页
//@------------------------------------起始页------------------------------------
//!------------------------------------主页------------------------------------
Route::rule('Index', 'Home/index', 'GET'); //?漫画 - 主页 - 首页
Route::rule('Banner', 'Home/Banner', 'GET'); //?漫画 - 主页 - 首页 - 横幅
Route::rule('Announcements', 'Home/announcements', 'GET'); //?漫画 - 主页 - 首页 - 公告
Route::rule('CategoryEveryoneSearch', 'Home/CategoryEveryoneSearch', 'GET'); //?漫画 - 分类 - 大家都在搜
Route::rule('PopularClassification', 'Home/PopularClassification', 'GET'); //?漫画 - 分类 - 所有分类
//!------------------------------------主页------------------------------------
//@------------------------------------漫画------------------------------------
Route::rule('ComicsList', 'Comics/Index', 'post'); //?漫画 - 分类 - 漫画列表
Route::rule('ComicsInfo', 'Comics/ComicsInfo', 'GET'); //?漫画 - 分类 - 漫画列表 - 漫画详细信息
Route::rule('ComicsAllChapter', 'Comics/AllChapter', 'GET'); //?漫画 - 分类 - 漫画列表 - 漫画详细信息 - 漫画章节
Route::rule('ComicsEpisodes', 'Comics/GetComicsProjectImagesList', 'GET'); //?漫画 - 分类 - 漫画列表 - 漫画详细信息 - 漫画章节 - 漫画章节信息
Route::rule('ComicsLike', 'Comics/like', 'POST'); //?喜欢
Route::rule('Recommendation', 'Comics/Recommendation', 'GET'); //?漫画 - 分类 - 漫画列表 - 漫画详细信息 - 大家都在看
//@------------------------------------漫画------------------------------------
//@------------------------------------评论------------------------------------
Route::rule('Comment/GetComment', 'Comment/Index', 'GET'); //?获取全部评论
Route::rule('Comment/CommentSonComment', 'Comment/SonComment', 'post'); //?获取其中一个评论的子评论
Route::rule('Comment/SendComment', 'Comment/SendComment', 'post'); //?发评论轮 / 回复评论
Route::rule('Comment/CommentLike', 'Comment/like', 'post'); //?喜欢/不喜欢某一个评论
//@------------------------------------评论------------------------------------
//!------------------------------------用户------------------------------------
Route::rule('UserLogin', 'User/Login', 'POST'); //?用户 - 登陆
Route::rule('User/Profile', 'User/profile', 'POST'); //?用户 - 基本信息
Route::rule('User/Favourite', 'User/favourite', 'POST'); //?用户 - 我的收藏
Route::rule('User/RegUser', 'User/RegUser', 'POST'); //?用户 - 注册
Route::rule('User/ActivateUser', 'User/ActivateUser', 'get'); //?用户 - 激活
Route::rule('User/RetrievePassword', 'User/RetrievePassword', 'POST'); //?用户 - 找回密码
Route::rule('User/ResetPassword', 'User/ResetPassword', 'get'); //?用户 - 找回密码 - 设置密码
//!------------------------------------用户------------------------------------
//@d ------------------------------------搜索------------------------------------
Route::rule('Comment/search', 'Search/Index', 'post'); //?发评论轮 / 回复评论
//@d ------------------------------------搜索------------------------------------