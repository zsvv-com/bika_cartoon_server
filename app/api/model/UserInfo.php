<?php
/*
 * @Date: 2020-07-03 15:27:44
 * @名称: 
 * @描述: 
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-20 13:41:35
 * @FilePath: /服务器/app/api/model/UserInfo.php
 * @最后编辑: 初雪桜
 */

namespace app\api\model;

use app\BaseController;
use think\facade\Db;

use think\Model;

class UserInfo extends Model
{
    //默认主键为自动识别，如果需要指定，可以设置属性：
    protected $readonly = ['_id', 'user_name']; //设置只读字段 防止更新
    protected $schema = [
        "id" => "int", //
        "user_name" => "string", // 用户名
        "pass_word" => "string", // 密码
        "birthday" => "datetime", // 生日
        "to_ken" => "string", // 令牌
        "application_time" => "datetime", // 注册时间
        "characters" => "string", // 爱好如 妹妹系，姐姐系
        "role" => "string", // 角色
        "exp" => "int", // 经验
        "slogan" => "string", // 个性签名
        "verified" => "tinyint", // 认证用户 真假
        "_id" => "string", // 随机生成的id
        "path" => "string", // 头像路径
        "original_name" => "string", // 头像名称
        "file_server" => "string", // 头像服务器地址
        "gender" => "int", // 性别 0机器人 1男性 2 女性
        "name" => "string", // 昵称
        "character" => "string", // 头像框url
        "exit_class_info" => "int", // 1' COMMENT '用户是否允许修改分类信息
        "images_update" => "int", // 是否允许上传图片
        "state" => "int", // 用户状态 1 正常 0异常
        "exp_state" => "int", // 异常原因 1封禁 2待认证
        "certification_time" => "datetime", // 认证时间
        "retrieve_password_key1" => "string", // 找回密码的key1
        "retrieve_password_key2" => "string", // 找回密码key2
        "retrieve_password" => "string", // 新设置密码 激活链接后吧此密码设置到pass_word
    ];
    /* // 设置当前模型的数据库连接
    protected $connect = [
        // 数据库类型
        'type'        => 'mysql',
        // 服务器地址
        'hostname'    => '127.0.0.1',
        // 数据库名
        'database'    => 'bica_acg',
        // 数据库用户名
        'username'    => 'root',
        // 数据库密码
        'password'    => 'hanhan521',
        // 数据库编码默认采用utf8
        'charset'     => 'utf8',
        // 数据库表前缀
        'prefix'      => '',
        // 数据库调试模式
        'debug'       => false,
    ]; */
}
