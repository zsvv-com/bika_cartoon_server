<?php
/*
 * @Author: your name
 * @Date: 2020-06-19 11:16:37
 * @LastEditTime: 2020-08-15 23:00:56
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /服务器/app/common.php
 */
// 应用公共文件

use PHPMailer\PHPMailer\PHPMailer;

/**
 * 发送邮件
 *
 * @param string $Dmail 收件地址
 * @param string $title 邮件标题
 * @param string $body 邮件内容
 * @return bool
 */
function sendMail($Dmail, $title, $body, $file = [])
{
    $mail = new PHPMailer();
    try {
        //邮件调试模式
        $mail->SMTPDebug = 0;
        //设置邮件使用SMTP
        $mail->isSMTP();
        // 设置邮件程序以使用SMTP
        $mail->Host = config("globalSettings.smtpSeverHost");
        // 设置邮件内容的编码
        $mail->CharSet = 'UTF-8';
        // 启用SMTP验证
        $mail->SMTPAuth = true;
        // SMTP username
        $mail->Username = config("globalSettings.smtpSeverUser");
        // SMTP password
        $mail->Password = config("globalSettings.smtpSeverPass");
        // 启用TLS加密，`ssl`也被接受
        $mail->SMTPSecure = 'tls';
        // 连接的TCP端口
        $mail->Port = 587;
        //设置发件人
        $mail->setFrom(config("globalSettings.smtpSeverMail"), config("globalSettings.smtpSeverName"));
        //  添加收件人1
        $mail->addAddress($Dmail);     // Add a recipient
        //            $mail->addAddress('ellen@example.com');               // Name is optional
        //            收件人回复的邮箱
        $mail->addReplyTo(config("globalSettings.smtpReplyAddress"), config("globalSettings.smtpSeverName"));
        //            抄送
        //            $mail->addCC('cc@example.com');
        //            $mail->addBCC('bcc@example.com');
        //附件
        if ($file) {
            foreach ($file as $key => $value) {
                $mail->addAttachment($value['fileSrc'], $value['fileName']);
            }
        }
        //            $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
        //            $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
        //Content
        // 将电子邮件格式设置为HTML
        $mail->isHTML(true);
        $mail->Subject = $title;
        $mail->Body    = $body;
        //            $mail->AltBody = '这是非HTML邮件客户端的纯文本';
        $mail->send();
        return true;
    } catch (Exception $e) {
        return false;
    }
}

/*
 * 加密解密
 *$str = 'abcdef';  
 * $key = 'www.helloweba.com';  
 * echo authcode($str,'ENCODE',$key,0); //加密  
 * $str = '56f4yER1DI2WTzWMqsfPpS9hwyoJnFP2MpC8SOhRrxO7BOk';  
 * echo authcode($str,'DECODE',$key,0); //解密  
 * 
 * @param string $string //要加密/解密的字符串
 * @param string $operation //加密/encode 解密/decode
 * @param string $key //密钥
 * @param integer $expiry
 * @return void
 */
function authcode($string, $operation = 'DECODE', $key = '', $expiry = 0)
{
    // 动态密匙长度，相同的明文会生成不同密文就是依靠动态密匙    
    $ckey_length = 4;

    // 密匙    
    $key = md5($key ? $key : config("globalsettings.serverLocalhost_Edit_EditComicsInfo_sha256Encryption"));

    // 密匙a会参与加解密    
    $keya = md5(substr($key, 0, 16));
    // 密匙b会用来做数据完整性验证    
    $keyb = md5(substr($key, 16, 16));
    // 密匙c用于变化生成的密文    
    $keyc = $ckey_length ? ($operation == 'DECODE' ? substr($string, 0, $ckey_length) : substr(md5(microtime()), -$ckey_length)) : '';
    // 参与运算的密匙    
    $cryptkey = $keya . md5($keya . $keyc);
    $key_length = strlen($cryptkey);
    // 明文，前10位用来保存时间戳，解密时验证数据有效性，10到26位用来保存$keyb(密匙b)，  
    //解密时会通过这个密匙验证数据完整性    
    // 如果是解码的话，会从第$ckey_length位开始，因为密文前$ckey_length位保存 动态密匙，以保证解密正确    
    $string = $operation == 'DECODE' ? base64_decode(substr($string, $ckey_length)) :  sprintf('%010d', $expiry ? $expiry + time() : 0) . substr(md5($string . $keyb), 0, 16) . $string;
    $string_length = strlen($string);
    $result = '';
    $box = range(0, 255);
    $rndkey = array();
    // 产生密匙簿    
    for ($i = 0; $i <= 255; $i++) {
        $rndkey[$i] = ord($cryptkey[$i % $key_length]);
    }
    // 用固定的算法，打乱密匙簿，增加随机性，好像很复杂，实际上对并不会增加密文的强度    
    for ($j = $i = 0; $i < 256; $i++) {
        $j = ($j + $box[$i] + $rndkey[$i]) % 256;
        $tmp = $box[$i];
        $box[$i] = $box[$j];
        $box[$j] = $tmp;
    }
    // 核心加解密部分    
    for ($a = $j = $i = 0; $i < $string_length; $i++) {
        $a = ($a + 1) % 256;
        $j = ($j + $box[$a]) % 256;
        $tmp = $box[$a];
        $box[$a] = $box[$j];
        $box[$j] = $tmp;
        // 从密匙簿得出密匙进行异或，再转成字符    
        $result .= chr(ord($string[$i]) ^ ($box[($box[$a] + $box[$j]) % 256]));
    }
    if ($operation == 'DECODE') {
        // 验证数据有效性，请看未加密明文的格式    
        if ((substr($result, 0, 10) == 0 || substr($result, 0, 10) - time() > 0) &&  substr($result, 10, 16) == substr(md5(substr($result, 26) . $keyb), 0, 16)) {
            return substr($result, 26);
        } else {
            return '';
        }
    } else {
        // 把动态密匙保存在密文里，这也是为什么同样的明文，生产不同密文后能解密的原因    
        // 因为加密后的密文可能是一些特殊字符，复制过程可能会丢失，所以用base64编码    
        return $keyc . str_replace('=', '', base64_encode($result));
    }
}

/**
 * curl Post文件，php5以下版本可用
 * 
 * @param $action (处理上传文件的url，form表单的action)
 * @param $path (文件路径)
 **/
function bilibiliUpdate($action, $path)
{

    $data = array(
        'file_up' => new CURLFile($path), //上传文件时候 必须使用这个类读取文件  
        'biz' => 'draw',
        'category' => 'daily',
        // 如果无效可以这样
        // 'pic'=>curl_file_create($path)
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_COOKIE, config("GlobalSettings.bilibiliUpdateImagesCookie")); //设置cookie
    curl_setopt($ch, CURLOPT_URL, $action);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);  //该curl_setopt可以向header写键值对
    curl_setopt($ch, CURLOPT_HEADER, false); // 不返回头信息
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $output = curl_exec($ch);
    curl_close($ch);
    return $output;
}

