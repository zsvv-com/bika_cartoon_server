<?php
/*
 * @Date: 2020-04-27 17:14:15
 * @名称: 
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-09 21:52:50
 * @FilePath: /服务器/app/serverLocalhost/route/route.php
 */

use think\facade\Route;

/* Route::get('index', function () {
    return 'hello,ThinkPHP666666!';
}); */

Route::rule('login', 'login/login', 'get'); //?用户登录
Route::rule('home', 'Home/Index', 'get'); //?后台框架
Route::rule('welcome', 'Home/Welcome', 'get'); //?后台框架
Route::rule('ComicsList', 'Comics/ComicsListView', 'get'); //?漫画列表
Route::rule('GetComicsList', 'Comics/GetComicsList', 'get'); //?ajax获取漫画列表
/* ajax获取漫画章节 需要post参数
 *  book_id post 
 */
Route::rule('GetComicsSonProject', 'Comics/GetComicsSonProject', 'post');

//@------------------------------------- 编辑类 -------------------------------------
/* 编辑章节
 * post参数 projectId 
 */

Route::rule('EditProject', 'Edit/EditProject', 'post'); //``true

/*  获取图片列表
 * post time 时间戳 注意60s超时
 * post encryption_bookId 书籍id 加密后
 * post encryption_projectId 章节id 加密后
 * post bookId 书籍id 加密前
 * post projectId 章节id 加密前
 */
Route::rule('GetImagesList', 'Edit/GetImagesList', 'post'); //获取章节信息 //``true

/* 提交章节
 * post time 时间戳 注意1h超时
 * post encryption_bookId 书籍id 加密后
 * post encryption_projectId 章节id 加密后
 * post bookId 书籍id 加密前
 * post projectId 章节id 加密前
 * post imageList 图片列表的json
 */
Route::rule('SubmissionProjectData', 'Edit/SubmissionProjectData', 'post'); //获取章节信息//``true

/* 编辑漫画信息
 * post time 时间戳 注意1h超时
 * post encryption_bookId 书籍id 加密后
 * post bookId 书籍id 加密前
 */
Route::rule('EditComicsInfo', 'Edit/EditComicsInfo', 'post'); //编辑漫画信息//``true

/* 创建章节分卷
 * post time 时间戳 注意1h超时
 * post encryption_bookId 书籍id 加密后
 * post bookId 书籍id 加密前
 */
Route::rule('AddProject', 'Edit/AddProject', 'post'); //创建章节分卷
//@------------------------------------- 编辑类 -------------------------------------

//!------------------------------------- 上传类 -------------------------------------
/* 图片上传
 * get  time 时间戳 注意1h超时
 * get  pictureCloudAcceleration 图片加速服务地址 sina/新浪 bilibili/哔哩哔哩 localhost本服务器
 * get  encryption_bookId 章节id sha256加密后 加密方法 config里的 serverLocalhost_Edit_sha256Encryption 加密顺序 书籍id+传入的时间 取sha256
 * get  encryption_projectId 章节id sha256加密后 加密方法 config里的 serverLocalhost_Edit_sha256Encryption 加密顺序 分卷id+传入的时间 取sha256
 * get  bookId 书籍id
 * get  projectId 分卷id
 */
Route::rule('ImagesUpdate', 'UpdateController/ImagesUpdate', 'post'); //上传章节漫画

/* 图片上传
 * get  time 时间戳 注意1h超时
 * get  pictureCloudAcceleration 图片加速服务地址 sina/新浪 bilibili/哔哩哔哩 localhost本服务器
 * get  encryption_bookId 章节id sha256加密后 加密方法 config里的 serverLocalhost_Edit_sha256Encryption 加密顺序 书籍id+传入的时间 取sha256
 * get  bookId 书籍id
 */
Route::rule('classCover', 'UpdateController/classCover', 'post'); //上传分类封面//``true

/* 图片上传
 * get  time 时间戳 注意1h超时
 * get  pictureCloudAcceleration 图片加速服务地址 sina/新浪 bilibili/哔哩哔哩 localhost本服务器
 * get  encryption_bookId 章节id sha256加密后 加密方法 config里的 serverLocalhost_Edit_sha256Encryption 加密顺序 书籍id+传入的时间 取sha256
 * get  bookId 书籍id
 */
Route::rule('bookCover', 'UpdateController/bookCover', 'post'); //上传漫画封面//!true
//!------------------------------------- 上传类 -------------------------------------

//@------------------------------------- 作者类 -------------------------------------
/* 修改作者信息
 * List initListType 参考数据 【0】//创作者【1】协作者
 * String initListUser_id// 一段json 使用 authcode 加密 出来 密钥 config("globalsettings.serverLocalhost_Edit_EditComicsInfo_sha256Encryption")
 * newListUser_id// 一段json 使用 authcode 加密 出来 密钥 config("globalsettings.serverLocalhost_Edit_EditComicsInfo_sha256Encryption")
 * toKen //先获取自己的token + 书籍id +时间戳 取sha256 然后比对这条记录是否一致
 * bookId//书籍id
 * time//时间戳
 * verificationBookId//书籍加密 书籍id+时间戳取sha256
 */
Route::rule('AuthorAndChineseTeam/Update', 'AuthorAndChineseTeam/Update', 'post');
/*
 * 审核用户
 * List initListType 参考数据 【0】//创作者【1】协作者
 * String initListUser_id// 一段json 使用 authcode 加密 出来 密钥 config("globalsettings.serverLocalhost_Edit_EditComicsInfo_sha256Encryption")
 * String newListUser_id// 一段json 使用 authcode 加密 出来 密钥 config("globalsettings.serverLocalhost_Edit_EditComicsInfo_sha256Encryption")
 * String toKen //先获取自己的token + 书籍id +时间戳 取sha256 然后比对这条记录是否一致
 * String bookId//书籍id
 * tint ime//时间戳
 * verificationBookId//书籍加密 书籍id+时间戳取sha256
 */
Route::rule('AuthorAndChineseTeam/InheritedUsersAreAllowedToJoin', 'AuthorAndChineseTeam/InheritedUsersAreAllowedToJoin', 'post');
/* 删除用户
 * List initListType 参考数据 【0】//创作者【1】协作者
 * String initListUser_id// 一段json 使用 authcode 加密 出来 密钥 config("globalsettings.serverLocalhost_Edit_EditComicsInfo_sha256Encryption")
 * String newListUser_id// 一段json 使用 authcode 加密 出来 密钥 config("globalsettings.serverLocalhost_Edit_EditComicsInfo_sha256Encryption")
 * String toKen //先获取自己的token + 书籍id +时间戳 取sha256 然后比对这条记录是否一致
 * String bookId//书籍id
 * tint ime//时间戳
 * verificationBookId//书籍加密 书籍id+时间戳取sha256
 */
Route::rule('AuthorAndChineseTeam/Delete', 'AuthorAndChineseTeam/Delete', 'post'); //``true

/* 同意删除用户 / 协作者申请踢出 管理者同意
 * List initListType 参考数据 【0】//创作者【1】协作者
 * String initListUser_id// 一段json 使用 authcode 加密 出来 密钥 config("globalsettings.serverLocalhost_Edit_EditComicsInfo_sha256Encryption")
 * String newListUser_id// 一段json 使用 authcode 加密 出来 密钥 config("globalsettings.serverLocalhost_Edit_EditComicsInfo_sha256Encryption")
 * String toKen //先获取自己的token + 书籍id +时间戳 取sha256 然后比对这条记录是否一致
 * String bookId//书籍id
 * tint ime//时间戳
 * verificationBookId//书籍加密 书籍id+时间戳取sha256
 */
Route::rule('AuthorAndChineseTeam/AgreeToDeleteUser', 'AuthorAndChineseTeam/AgreeToDeleteUser', 'post'); //``true
/* 拒绝删除用户 / 协作者申请踢出 管理者拒绝
 * List initListType 参考数据 【0】//创作者【1】协作者
 * String initListUser_id// 一段json 使用 authcode 加密 出来 密钥 config("globalsettings.serverLocalhost_Edit_EditComicsInfo_sha256Encryption")
 * String newListUser_id// 一段json 使用 authcode 加密 出来 密钥 config("globalsettings.serverLocalhost_Edit_EditComicsInfo_sha256Encryption")
 * String toKen //先获取自己的token + 书籍id +时间戳 取sha256 然后比对这条记录是否一致
 * String bookId//书籍id
 * tint ime//时间戳
 * verificationBookId//书籍加密 书籍id+时间戳取sha256
 */
Route::rule('AuthorAndChineseTeam/RefuseToDeleteUser', 'AuthorAndChineseTeam/RefuseToDeleteUser', 'post'); //``true
/* 恢复用户 / 因为被踢出 管理者恢复 //自己退出无效
 * List initListType 参考数据 【0】//创作者【1】协作者
 * String initListUser_id// 一段json 使用 authcode 加密 出来 密钥 config("globalsettings.serverLocalhost_Edit_EditComicsInfo_sha256Encryption")
 * String newListUser_id// 一段json 使用 authcode 加密 出来 密钥 config("globalsettings.serverLocalhost_Edit_EditComicsInfo_sha256Encryption")
 * String toKen //先获取自己的token + 书籍id +时间戳 取sha256 然后比对这条记录是否一致
 * String bookId//书籍id
 * tint ime//时间戳
 * verificationBookId//书籍加密 书籍id+时间戳取sha256
 */
Route::rule('AuthorAndChineseTeam/RecoveryUser', 'AuthorAndChineseTeam/RecoveryUser', 'post'); //``true

/* 确认增加用户
 * "initListType": [0],  参考数据 【0】//创作者【1】协作者
 * "AddUser_id": this.getValue('valueStr'), //要添加的用户数据
 * "toKen": me_token, //用户token
 * "bookId": bookId,//书籍id
 * "time": time,//时间戳
 * "verificationBookId": verificationBookId,//书籍加密 书籍id+时间戳取sha256
 */
Route::rule('AuthorAndChineseTeam/EnterAddUser', 'AuthorAndChineseTeam/EnterAddUser', 'post'); //``true

/* 创作者同意协作者加入的用户
 * "initListType": [0],  参考数据 【0】//创作者【1】协作者
 * "AddUser_id": this.getValue('valueStr'), //要添加的用户数据
 * "toKen": me_token, //用户token
 * "bookId": bookId,//书籍id
 * "time": time,//时间戳
 * "verificationBookId": verificationBookId,//书籍加密 书籍id+时间戳取sha256
 */
Route::rule('AuthorAndChineseTeam/AllowToJoin', 'AuthorAndChineseTeam/AllowToJoin', 'post'); //``true


/* 创作者拒绝协作者加入的用户
 * "initListType": [0],  参考数据 【0】//创作者【1】协作者
 * "AddUser_id": this.getValue('valueStr'), //要添加的用户数据
 * "toKen": me_token, //用户token
 * "bookId": bookId,//书籍id
 * "time": time,//时间戳
 * "verificationBookId": verificationBookId,//书籍加密 书籍id+时间戳取sha256
 */
Route::rule('AuthorAndChineseTeam/RefuseToJoin', 'AuthorAndChineseTeam/RefuseToJoin', 'post'); //``true

/*保存文章信息
 * 
 */
Route::rule('AuthorAndChineseTeam/SaveInformation', 'AuthorAndChineseTeam/SaveInformation', 'post'); //``true
//@------------------------------------- 作者类 -------------------------------------

//!------------------------------------- 搜索类 -------------------------------------
/* 查找用户
 * keyword 用户关键字 只能是用户名的 且大于7位
 */
Route::rule('SelectController/selectUserInfo', 'SelectController/selectUserInfo', 'post'); //``true
/* 查找标签
 * keyword 用户关键字
 */
Route::rule('SelectController/SelectTags', 'SelectController/SelectTags', 'post');
//!------------------------------------- 搜索类 -------------------------------------
//@------------------------------------- 创建类 -------------------------------------
Route::rule('Create/Comics', 'CreateComics/CreateComics', 'get');
/* 保存漫画
 * "projectname" post 漫画名
 * "ComicsInfo" post //漫画简介
 * "comment" post //允许评论
 * "download" post //允许下载
 * "hashHmac_toKen" post //加密的验证信息
 */
Route::rule('Create/SeverComics', 'CreateComics/SeverComics', 'post');
//@------------------------------------- 创建类 -------------------------------------
//!------------------------------------- 文章类 -------------------------------------
/* 恢复漫画
 * "bookId" 书籍id,
 * "title" 书籍名,
 * "token" 用户token,
 */
Route::rule('ComicsOperation/RecoveryComics', 'CreateComics/RecoveryComics', 'post'); //``true

/* 删除漫画
 * "bookId" 书籍id,
 * "title" 书籍名,
 * "token" 用户token,
 */
Route::rule('ComicsOperation/DeleteComics', 'CreateComics/DeleteComics', 'post'); //``true

/* 永久删除漫画
 * "bookId" 书籍id,
 * "title" 书籍名,
 * "token" 用户token,
 */
Route::rule('ComicsOperation/EnterDeleteComics', 'CreateComics/EnterDeleteComics', 'post');
//!------------------------------------- 文章类 -------------------------------------
//@------------------------------------- 分类和标签 -------------------------------------
/*分类管理 
 */
Route::rule('Classified/ClassifiedCatalogue', 'ClassifiedAndTags/ClassifiedCatalogue', 'get'); //分类列表
/*编辑分类页面
 *token 用户token 
 *title  分类标题
 *class_id  分类id
 *回自己判断是否存在数据 不存在就进入创建模式
 */
Route::rule('Classified/EditClassified', 'ClassifiedAndTags/EditClassified', 'get'); //编辑分类数据

/* 保存分类数据
 * post token 
 * get class_id 分类id
 * post title 标题
 * post description 
 * post is_web 是否为网页
 * post link 链接
 * post is_top //是否置顶
 * post is_enable //是否启用
 * post coverList //封面
 * post establish //是否创建漫画
 */
Route::rule('Classified/PreservationClassInfo', 'ClassifiedAndTags/PreservationClassInfo', 'post'); //保存分类数据

/* 启用禁用分类
 * post token 
 * get class_id 分类id
 * post title 标题
 * id 数据库id
 */
Route::rule('Classified/updateState', 'ClassifiedAndTags/updateState', 'post'); //保存分类数据
/* 删除分类
 * post token 
 * get class_id 分类id
 * post title 标题
 * id 数据库id
 */
Route::rule('Classified/deleteAndRecoveryClass', 'ClassifiedAndTags/deleteAndRecoveryClass', 'post'); //删除分类
/* 永久删除分类
 * post token 
 * get class_id 分类id
 * post title 标题
 * id 数据库id
 */
Route::rule('Classified/enterDeleteClass', 'ClassifiedAndTags/enterDeleteClass', 'post'); //删除分类

/*标签管理 
 */
Route::rule('Classified/TagCatalogue', 'ClassifiedAndTags/TagCatalogue', 'get'); //分类列表
/*编辑分类页面
 *token 用户token 
 *title  分类标题
 *class_id  分类id
 *回自己判断是否存在数据 不存在就进入创建模式
 */

/* 启用禁用分类
 * post token 
 * get class_id 分类id
 * post title 标题
 * id 数据库id
 */
Route::rule('Classified/TagCatalogue/updateState', 'ClassifiedAndTags/TagCatalogueUpdateState', 'post'); //保存分类数据

/* 删除标签
 * post token 
 * get class_id 分类id
 * post title 标题
 * id 数据库id
 */
Route::rule('Classified/TagCatalogue/tagDelete', 'ClassifiedAndTags/tagDelete', 'post'); //保存分类数据
//@------------------------------------- 分类和标签 -------------------------------------
//!------------------------------------- 数 据 优化 -------------------------------------
Route::rule('DataOptimization/index', 'DataOptimization/index', 'get'); //删除分类
Route::rule('DataOptimization/DeletedCategorie', 'DataOptimization/DeletedCategorie', 'get'); //清空已删除分类数据

Route::rule('DataOptimization/FileManagementController', 'DataOptimization/FileManagementController', 'get'); //删除残余数据
Route::rule('DataOptimization/FileManagementController/deleteRedundantDocuments', 'DataOptimization/deleteRedundantDocuments', 'post'); //删除残余数据
Route::rule('DataOptimization/FileManagementController/DeleteMissingFiles', 'DataOptimization/DeleteMissingFiles', 'post'); //删除残余数据
// Route::rule('DataOptimization/index', 'DataOptimization/index', 'get'); //首页
// Route::rule('DataOptimization/FindDuplicateFiles', 'DataOptimization/runTaks', 'post'); //删除分类
//!------------------------------------- 数 据 优化 -------------------------------------
