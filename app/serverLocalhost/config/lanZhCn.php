<?php
/*
* @Date: 2020-07-06 23:32:36
* @名称: 语言文件
* @描述: 中文
* @版本: 0.01
* @作者: 初雪桜
* @邮箱: 202184199@qq.com
* @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-08 17:09:47
 * @FilePath: /服务器/app/serverLocalhost/config/lanZhCn.php
* @最后编辑: 初雪桜
*/
return [
    //*其他类
    "Other-10000" => "超时,请重新登录", //超时,请重新登录
    //*其他类
    //``用户类
    "User-10000" => "发现多个重复用户请联系管理员修复", //用户验证时查询数据库 发现多个用户
    "User-10001" => "用户验证失败", //用户验证失败,查找不到用户
    "User-10002" => "查找用户失败", //根据用户传入的信息寻找不到用户
    "User-10003" => "用户状态异常", //封禁 等异常
    "User-10004" => "用户权限不足", //封禁 等异常
    //``用户类
    //!令牌类
    "Token-10000" => "token错误", //token为三段设计 验证时候 超过三段 或不到 则会出发
    "Token-10001" => "token解析失败", //token所传入的 第一段第二段 解析失败
    "Token-10002" => "token验证失败", //token 第二段 加密后的结果 和第三段不一致
    //!令牌类
    //#漫画类
    "Comics-10000" => "漫画分卷id重复联系管理员删除", //创建漫画时生成的漫画id重复
    "Comics-10001" => "没有找到漫画信息", //根据漫画的id+分卷id 寻找不到漫画
    "Comics-10002" => "无效的id",
    "Comics-10003" => "漫画验证错误",
    //#漫画类
    //&分类类
    "ClassifiedAndTags-EnterDeleteClass-10001" => "删除漫画需要先清空分类需要请前往数据优化进行操作",
    //&分类类
    //todo 这是指向类 指定在某个文件中使用的 AuthorAndChineseTeam 中的 EnterAddUser
    //!AuthorAndChineseTeam
    //#EnterAddUser 确认增加用户
    "AuthorAndChineseTeam_EnterAddUser-User-10000" => "解析传入用户 信息失败请刷新网页重试", //AuthorAndChineseTeam
    "AuthorAndChineseTeam_EnterAddUser-User-10001" => "用户权限错误 请刷新网页再试", //!查询用户其中的某个必要权限时不可用
    "AuthorAndChineseTeam_EnterAddUser-User-10002" => "添加用户失败 用户已存在 ", //!用户已被添加
    "AuthorAndChineseTeam_EnterAddUser-User-10003" => "无法激活处于 另一种状态的用户", //另一个用户异常 或当前用户异常
    //#EnterAddUser 确认增加用户
    //*PermissionQuery 用户权限查询
    "AuthorAndChineseTeam_PermissionQuery-10000" => "用户多次出现在此漫画创作者/协作者中 请联系管理员解决", //当同一本漫画 我作为漫画的创作者/协作者多次出现 会触发
    "AuthorAndChineseTeam-PermissionQuery-10001" => "该用户不存在于此漫画 请先添加", //查询某个用户 于当前漫画权限 用户不存在时出现
    //*PermissionQuery 用户权限查询
    //?Delete 删除用户
    "AuthorAndChineseTeam-Delete-10000" => "如果要删除用户请不要修改任何内容", //删除用户时候修改了用户信息
    "AuthorAndChineseTeam-Delete-10001" => "请先指定一个新的作者", //退出项目 时没有新作者 只有或没有协作者
    "AuthorAndChineseTeam-Delete-undefined" => "发生意外错误 未指定错误", //发生意外错误
    //?Delete 删除用户
    // RecoveryUser 恢复用户
    "AuthorAndChineseTeam-RecoveryUser-10000" => "当前用户权限错误", //只有创作者才有恢复用户权限 当前用户异常
    "AuthorAndChineseTeam-RecoveryUser-10001" => "没有这个用户", //恢复用户时候被恢复的用户 不存在 或者 用户状态正常
    // RecoveryUser 恢复用户
    //!AuthorAndChineseTeam
    //!updateController//图片上传
    "updateController-Comics-10000" => "漫画验证错误", //只有创作者才有恢复用户权限 当前用户异常
    "updateController-Save-10000" => "文件保存失败", //上传图片文件保存是阿碧 一般为权限问题服务器权限
    "updateController-Save-10001" => "上传的服务器节点未选择",
    "updateController-Save-10002" => "无效id", //被上传的漫画查询id时候查询不到这个漫画
    "updateController-Save-10003" => "无效扩展名", //被上传的漫画查询id时候查询不到这个漫画
    "updateController-Save-10004" => "未选择文件", //被上传的漫画查询id时候查询不到这个漫画
    //!updateController//图片上传
    //?DataOptimization 站点优化
    //!deleteRedundantDocuments 删除重复文件
    "deleteRedundantDocuments-10001" => "没有访问此文件权限",
    "deleteRedundantDocuments-10002" => "被删除信息不匹配", //删除多余文件 却在数据库中寻找到了 被删除的数据 不能删除
    "deleteRedundantDocuments-10003" => "被删除文件不正确", //删除多余文件 只支持jpeg  png  gif
    "deleteRedundantDocuments-10004" => "删除文件失败", //所删除的文件 不能被删除 一般出现在linux 检查权限
    "deleteRedundantDocuments-10005" => "被删除文件不存在", //所删除的文件 不能被删除 一般出现在linux 检查权限
    "deleteRedundantDocuments-Database-10001" => "数据库定位错误", //删除文件时候 定位的数据库不存在
    //!deleteRedundantDocuments 删除重复文件
    //*DeleteMissingFiles 删除重复文件
    "deleteRedundantDocuments-10006" => "数据库定位错误", //删除已丢失文件 数据库查询不到这个文件信息返回
    "deleteRedundantDocuments-10007" => "文件尚未丢失", //删除已丢失文件 查询到这个文件未丢失
    //*DeleteMissingFiles 删除重复文件

    //?DataOptimization 站点优化
];
