<?php
/*
 * @Date: 2020-05-03 22:03:52
 * @名称: 
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-24 23:51:14
 * @FilePath: /服务器/app/serverLocalhost/controller/Comics.php
 */

declare(strict_types=1);

namespace app\serverLocalhost\controller;

use app\BaseController;
use think\facade\Db;
use think\facade\View;

class Comics extends BaseController
{
    public function ComicsListView()
    {
        $token = cookie("Authorization");;
        $userTokenVerificationParse = ParseUserToken($token, true);
        if ($userTokenVerificationParse['code'] != 200) {
            return json($userTokenVerificationParse);
        }
        //用户信息
        $page = input('get.page', 1); //获取当前页
        $seleNumber = 20; //每次查询的数量
        $data_list =  Db::table('book_list')
            ->where([
                "book_list.state" => 1,
                "delete" => 0, //没有被删除的
            ])
            ->limit($page * $seleNumber - $seleNumber, $seleNumber)
            ->alias('book_list')
            ->join('user_info user_info', 'user_info._id = book_list.user_id')
            ->Join('book_info book_info', 'book_list._id = book_info._id')
            ->order('book_list.id desc')
            ->field([
                "book_list.id", //在数据库id
                "book_list.title", //标题
                "book_list._id", //漫画id
                "book_info.categories", //分类
                "book_info.tags", //分类
                "book_list.created_at", //创建时间
                "book_list.file_server", //服务器地址
                "book_list.path", //路径
                "user_info.name as author", //作者姓名
            ])
            ->select();
        $data_list_count = Db::table('book_list')
            ->where([
                "state" => 1,
                "delete" => 0, //没有被删除的
            ])
            ->count();
        return view(
            'Comics/ComicsList/ComicsList',
            [
                "comicsList" => $data_list,
                "ComicsListCount" => "$data_list_count",
                "page" => $page,
                'springboard' => config("globalsettings.PictureSpringboard"),
                "url" => 'http://' . $_SERVER['HTTP_HOST'] . "/",
            ]
        );
    }
    public function getComicsList()
    #获取漫画列表 json
    {
        sleep(1);
        $page = input('get.page', 1);
        #获取当前页
        $title = input('get.title', null);
        #要搜索的标题
        $status = input('get.status', 0);
        #要跳转的页数
        $selectWhere = [];
        array_push($selectWhere, ["book_list.delete", '=', 0]); //不能被删除
        if ($title != null && $title != "") {
            array_push($selectWhere, ["book_list.title", 'like', "%$title%"]);
            $ExtendedParameters = ",$title";
        } else {
            $ExtendedParameters = ",undefined";
        }
        switch ($status) {
            case 0:
                #正常的
                array_push($selectWhere, ["book_list.state", '=', 1]);
                $ExtendedParameters .= "";
                break;
            case 1:
                #待审核
                array_push($selectWhere, ["book_list.state", '=', 0]);
                array_push($selectWhere, ["book_list.exp_state", '=', 1]);
                break;
            case 2:
                #待删除
                array_push($selectWhere, ["book_list.state", '=', 0]);
                array_push($selectWhere, ["book_list.exp_state", '=', 2]);
                break;
            default:
                return errorJsonReturn("Parameter validation failed");
                break;
        }
        $seleNumber = 20;
        //每次查询的数量
        try {
            $data_list =  Db::table('book_list')
                ->where($selectWhere)
                ->limit($page * $seleNumber - $seleNumber, $seleNumber)
                ->alias('book_list')
                ->join('user_info user_info', 'user_info._id = book_list.user_id')
                ->join('book_info book_info', 'book_info._id = book_list._id')
                ->field([
                    "book_list.title",
                    #标题
                    "book_list._id",
                    #漫画id
                    "book_info.categories",
                    #分类
                    "book_info.tags",
                    #标签
                    "book_list.created_at",
                    #创建时间
                    "book_list.file_server",
                    #服务器地址
                    "book_list.path",
                    #路径
                    "book_list.state",
                    #状态 1正常 0异常
                    "book_list.exp_state",
                    #扩展状态 1待站长审核 2待删除
                    "user_info.name as author",
                    #作者姓名
                ])
                ->order('book_list.id desc')
                ->select();
        } catch (\Throwable $th) {
            dump($th);
        }

        $data_list_count =  Db::table('book_list')
            ->where($selectWhere)
            ->count();
        // dump($data_list);
        // return view('Comics/ComicsList/ComicsList', ["comicsList" => $data_list, 'springboard' => "http://111.beaa.xyz/?url="]);
        return json([
            "comicsList" => $data_list,
            "ComicsListCount" => $data_list_count,
            "url" => 'http://' . $_SERVER['HTTP_HOST'] . "/",
            "page" => generatePaging($data_list_count, $page, $ExtendedParameters),
            'springboard' => config("globalsettings.PictureSpringboard"),
        ]);
    }
    /**
     * 获取漫画章节信息
     *
     * @return json
     */
    public function getComicsSonProject()
    {
        // sleep(1);
        $bookId = input('post.bookId', null); //获取当前页
        $comicsSonProject =  Db::table('book_info_project')
            ->where([
                "_id" => $bookId,
            ])
            ->field([
                "project_title", //分卷名称
                "chapter_id", //分卷id
                "updated_at", //更新时间
                "order", //分卷章节
            ])
            ->select();
        $comicsSonProject_count =  Db::table('book_info_project')
            ->where([
                "_id" => $bookId,
            ])
            ->count();
        return json([
            "comicsSonProject" => $comicsSonProject,
            "comicsSonProjectCount" => $comicsSonProject_count,
        ]);
    }
}
