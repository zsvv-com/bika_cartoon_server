<?php
/*
 * @Date: 2020-05-03 22:03:52
 * @名称: 搜索用户信息
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-26 22:52:42
 * @FilePath: /Pica_acg服务器/app/serverLocalhost/controller/SelectController.php
 */

namespace app\serverLocalhost\controller;

use think\facade\Db;
use think\facade\View;

class SelectController
{
    public function selectUserInfo()
    {
        $token = cookie("Authorization");;
        $userTokenVerificationParse = ParseUserToken($token, true);
        if ($userTokenVerificationParse['code'] != 200) {
            return json($userTokenVerificationParse);
        }

        $keyWord = input('keyword', " ");
        $search = array(" ", "　", "\n", "\r", "\t");
        $replace = array("", "", "", "", "");
        $keyWord = str_replace($search, $replace, $keyWord);

        $postData['toKen'] = input('post.toKen'); //toKen我自己的
        $postData['bookId'] =  input('post.bookId'); //书籍id
        $postData['time'] = input('post.time'); //时间戳
        $postData['verificationBookId'] = input('post.verificationBookId'); //sha256的书籍id

        if (strlen($keyWord) >= 7) { //查询条件为7位起步
            $return_data = Db::table('user_info')
                ->where(
                    "user_name",
                    'like',
                    "%$keyWord%"
                )
                ->alias('user_info')
                ->join('author_plan author_plan', 'author_plan.user_id = user_info._id')
                ->field([
                    'user_info.user_name',
                    'user_info._id',
                ])
                ->select();
            if (count($return_data) > 5) { //判断预选用户有几个
                return errorJsonReturn("Continue to fill in keywords",);
            } else { //返回用户数据
                $hashHmac_toKen = hash_hmac("sha256", $token .  $postData['bookId'] . $postData['time'], config("globalsettings.serverLocalhost_Edit_EditComicsInfo_sha256Encryption")); //加密后 加密id
                $hashHmac_bookId = hash_hmac("sha256",   $postData['bookId'] . $postData['time'], config("globalsettings.serverLocalhost_Edit_EditComicsInfo_sha256Encryption")); //加密后 加密id
                if ($hashHmac_toKen == $postData['toKen']) { //判断加密的token是不是通过
                    if ($hashHmac_bookId == $postData['verificationBookId']) { //验证书籍id
                        $user_list = [];
                        $date =  date("Y-m-d H:i:s");
                        foreach ($return_data as $key => $value) {
                            $user_id = [

                                "hash_hmac_user_id" => hash_hmac('sha256', $value["_id"] . $postData['bookId'] . $date, config("globalsettings.serverLocalhost_Edit_EditComicsInfo_sha256Encryption")),
                                "user_id" => $value["_id"],
                                "time" => $date,
                            ];
                            $user_list[$key] = [
                                "name" => $value["user_name"],
                                "value" => authcode(base64_encode(json_encode($user_id)), 'encode', config("globalsettings.serverLocalhost_Edit_EditComicsInfo_sha256Encryption")),
                                "selected" => false,
                                "disabled" => false,
                            ];
                        }
                        return json_decode(successJsonReturn($user_list)->getContent(), true);
                    } else {
                        return errorJsonReturn('Comics-10003'); //到这里就是验证错误 验证书籍错误
                    }
                } else {
                    return errorJsonReturn('Token-10002', 'SeverError'); //到这里就是验证错误 验证书籍错误
                }
            }
        } else {
            return errorJsonReturn("Search requirements not met",);
        }
    }
    public function SelectTags()
    {
        $user_info = config("globalsettings.user_info"); //用户信息

        $keyWord = input('keyword', " ");
        $search = array(" ", "　", "\n", "\r", "\t");
        $replace = array("", "", "", "", "");
        $keyWord_a = str_replace($search, $replace, $keyWord);
        if ($keyWord_a == "") {
            return errorJsonReturn('请填写关键字'); //到这里就是验证错误 验证书籍错误
        } else {
            unset($keyWord_a);
        }
        $postData['toKen'] = input('post.toKen'); //toKen我自己的
        $postData['bookId'] =  input('post.bookId'); //书籍id
        $postData['time'] = input('post.time'); //时间戳
        $postData['verificationBookId'] = input('post.verificationBookId'); //sha256的书籍id
        $comicsTagList = [];
        $TagList = Db::table('tags')
            ->where(
                "tag_name",
                'like',
                "%$keyWord%"
            )
            ->select();
        if (count($TagList) == 0) {

            array_push($comicsTagList, [
                "name" =>  "创建标签:$keyWord -",
                "value" => $keyWord,
            ]);
            return successJsonReturn($comicsTagList);
        }

        foreach ($TagList as $key => $value) {
            array_push($comicsTagList, [
                "name" => "使用标签: $value[tag_name] -",
                "value" => $value["tag_name"],
            ]);
        }
        return successJsonReturn($comicsTagList);
    }
}
