<?php
/*
 * @Date: 2020-05-03 22:03:52
 * @名称: 上传控制器
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-08-19 20:08:31
 * @FilePath: /服务器/app/serverLocalhost/controller/login.php
 */

namespace app\serverLocalhost\controller;

use CURLFile;
use sina\sina;
use think\facade\Db;
use think\facade\View;

class Login
{
    public function login()
    {
        $userName = "202184199"; //登录账号
        $passWord = "12345678"; //登录密码
        $platform = "web"; //登录模式 web模式
        $userState = Db::table('admin_user_info')
            ->where([
                "user_name" => $userName,
                "pass_word" => $passWord,
            ])
            ->select();
        // {"_id":"5af393a7db91d02c83d987b4","email":"202184199","role":"member","name":"鞠耀斌","version":"2.1.2.3","buildVersion":"32","platform":"ios","iat":1582973412,"exp":1583578212}
        $token = json_encode([
            "_id" => $userState[0]['_id'], //用户id
            "email" => $userName, //邮箱
            "role" => $userState[0]['role'], //身份
            "name" => $userState[0]['name'], //昵称
            "platform" => $platform, //系统版本 ios/android/web
            "iat" => time(), //申请时间
            "exp" => time() + 604800, //过期时间
        ]);
        $EncryptionMethod = json_encode([ //加密方式
            "Encryption" => "SHA256", //加密方法 假的误导用 
            "verification" => "SHA512",
        ]);
        $str1 = base64_encode($EncryptionMethod); //头部验证信息
        $str2 = base64_encode(authcode($token, 'encode', config('serverConfig.TokenResolutionKey'))); //加密字符串
        $str3 = hash_hmac("sha512", $str2, config('serverConfig.TokenResolutionKeySha512')); //取 加密字符串的 sha512

        $data["message"] = "success";
        $data["data"]["token"] = "$str1.$str2.$str3";
        $data["code"] = "200";
        cookie("Authorization", "$str1.$str2.$str3");
        Db::table('admin_user_info')
            ->where($userState[0])
            ->update([
                "to_ken" => "$str1.$str2.$str3"
            ],);
    }
}
function asc2bin($temp)
{
    $len = strlen($temp);
    $data = '';
    for ($i = 0; $i < $len; $i++) {
        $data .= sprintf('%08b', ord(substr($temp, $i, 1)));
    }
    return $data;
}
