<?php
/*
 * @Date: 2020-05-03 22:03:52
 * @名称: 
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-07-25 20:50:26
 * @FilePath: /Pica_acg服务器/app/serverLocalhost/controller/Home.php
 */

namespace app\serverLocalhost\controller;

use think\facade\View;

class Home
{
    public function index()
    {

        View::assign('serverLocalhost',"serverLocalhost");
        return View::fetch('Home/Home/Index');
    }
    public function welcome()
    {
        return View::fetch('Home/Welcome/welcome');
    }
}
