# 2020年10月12日 push1
* 更新概要

    > * 删除评论区 展示的字评论 
    > * bug 点击 漫画详细信息 服务器会出现 500 待解决

# 2020年9月14日 push1

* 更新概要 

    > * 分解了 用户控制器 全部 

![#f03c15](https://placehold.it/15/f03c15/000000?text=+)

 `至此所有的User 区域代码优化完成`
# 2020年9月13日 push3

* 更新概要 

    > * 控制器home控制器的 获取横幅
    > * 控制器User控制器的 用户登录、修复了用户注册 在 表 user_info_table_name 不插入数据问题 注册实现检测每个用户所在的表

# 2020年9月13日 push2

    > * 控制器comicsInfo 被合并到了comics 中 

* 更新概要 

    > * 分解了 漫画控制器的 漫画推荐、喜欢/不喜欢蛮糊 

![#f03c15](https://placehold.it/15/f03c15/000000?text=+)

 `至此所有的comics 区域代码优化完成`
# 2020年9月13日

* 更新概要 

    > * 分解了 漫画控制器的 获取分类漫画列表、漫画详细信息、获取分卷、

# 2020年9月12日

* 更新概要 

    > * 分解了 评论区的查看评论、查看子评论、用户喜欢/不喜欢

![#f03c15](https://placehold.it/15/f03c15/000000?text=+)

 `至此所有的comment 区域代码优化完成`
# 2020年9月10日教师节

1. 修复了评论 回复子评论 出现500错误的问题 已知问题 不能点赞 
2. 期望优化 获取评论时候 获取五个子评论 

 
 
 !!! 注意 
 api / ComicsInfo ///#todo "is_path" 已被修改 请修改客户端
 
 ### 切割数据库
 ini_set('memory_limit', '-1'); 

        set_time_limit(0);
        $data_list = Db::table('book_info_project')
            ->order('_id asc and chapter_id asc')
            ->select();
        $booklist = [];
        $bookListKey = 0;
        foreach ($data_list as $key => $value) {
            $bookInfoList = Db::table('book_info_project_info_delete')
                ->where([
                    "_id" => $value['_id'],
                    "project_id" => $value['chapter_id'],
                ])
                ->select();
            Db::table('book_info_project_info_delete') //查询完直接删除
                ->where([
                    "_id" => $value['_id'],
                    "project_id" => $value['chapter_id'],
                ])
                ->delete();
            if (!isset($booklist[$bookListKey])) {
                $booklist[$bookListKey] = [
                    "count" => 0,
                    "list" => [],
                ];
            }
            if ($booklist[$bookListKey]['count'] + count($bookInfoList) < 50000) {
                foreach ($bookInfoList as $key => $SonSonValue) {
                    # code...
                    $booklist[$bookListKey]['list'][] = $SonSonValue;
                }
                $booklist[$bookListKey]['count'] = $booklist[$bookListKey]['count'] + count($bookInfoList);
            } else {
                $bookListKey++;
                if (!isset($booklist[$bookListKey])) {
                    $booklist[$bookListKey] = [
                        "count" => 0,
                        "list" => [],
                    ];
                }
                foreach ($bookInfoList as $key => $SonSonValue) {
                    # code...
                    $booklist[$bookListKey]['list'][] =   $SonSonValue;
                }
                $booklist[$bookListKey]['count'] = $booklist[$bookListKey]['count'] + count($bookInfoList);
            }
        }

        foreach ($booklist as $key => $value) {
            $tableName = "book_info_project_info_$key";
            $sql = "CREATE TABLE `book_info_project_info_$key` (

`id` int(11) NOT NULL AUTO_INCREMENT, 
`frame_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '图片id', 
`original_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '文件名', 
`path` varchar(5000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL, 
`file_server` varchar(5000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL, 
`project_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '分卷id', 
`title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL, 
`_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '书籍id', 
`is_path` int(1) DEFAULT '1' COMMENT '是否需要使用path', 

                PRIMARY KEY ( `id` )
              ) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='漫画章节图片表第 $key 张表';";
            DB::query($sql);
            foreach ($value['list'] as $key => $sonValue) {

                Db::table('book_info_project')
                    ->where([
                        "chapter_id" => $sonValue['project_id'],
                        "_id" => $sonValue['_id']
                    ])
                    ->update(["database_table_name" => $tableName]);
                Db::table("$tableName")
                    ->insert([
                        "frame_id" => $sonValue['frame_id'],
                        "original_name" => $sonValue['original_name'],
                        "path" => $sonValue['path'],
                        "file_server" => $sonValue['file_server'],
                        "project_id" => $sonValue['project_id'],
                        "title" => $sonValue['title'],
                        "_id" => $sonValue['_id'],
                        "is_path" => $sonValue['is_path'],
                    ]);
            }
        }

        return;

### 统计漫画分卷图片

        ini_set('memory_limit', '-1');
        set_time_limit(0);
        $list = Db::table('book_info_project')
            ->where("database_table_name", "<>", null)
            ->select();
        foreach ($list as $key => $value) {
            $count = Db::table($value['database_table_name'])
                ->where([
                    "_id" => $value['_id'],
                    "project_id" => $value["chapter_id"],
                ])
                ->count();
            Db::table("book_info_project")
                ->where([
                    "id"=>$value['id']
                ])
                ->update([
                    "images_count" => $count
                ]);
        }

        return;
