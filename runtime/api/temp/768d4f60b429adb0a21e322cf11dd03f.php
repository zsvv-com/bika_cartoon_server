<?php /*a:1:{s:62:"/Users/juyaobin/Desktop/Site/app/api/view/Success/Success.html";i:1592838410;}*/ ?>
<!--
 * @Date: 2020-05-10 12:31:51
 * @名称: 
 * @版本: 0.01
 * @作者: 初雪桜
 * @邮箱: 202184199@qq.com
 * @最后编辑人: 初雪桜
 * @LastEditTime: 2020-06-22 23:06:50
 * @FilePath: /Pica_acg服务器/app/api/view/Success/Success.html
 -->
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>成功</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link rel="shortcut icon" href="favicon.ico">
    <link href="/public/static/qadmin/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="/public/static/qadmin/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="/public/static/qadmin/css/animate.css" rel="stylesheet">
    <link href="/public/static/qadmin/css/style.css?v=4.1.0" rel="stylesheet">
</head>

<body class="gray-bg">


    <div class="middle-box text-center animated fadeInDown">
        <h1><?php echo htmlentities($errorCode); ?></h1>
        <h3 class="font-bold"><?php echo htmlentities($errorTitle); ?></h3>

        <div class="error-desc">
            <br /><a href="<?php echo htmlentities($url); ?>" class="btn btn-primary m-t"><?php echo htmlentities($urlTitle); ?></a>
        </div>
    </div>

    <!-- 全局js -->
</body>

</html>